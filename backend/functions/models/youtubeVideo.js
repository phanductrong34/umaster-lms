class youtubeVideo {
    constructor(title, id, vidId, channelId, thumbnail, position, channelName, description,type){
        this.title = title;
        this.id = id,
        this.vidId = vidId,
        this.channelId = channelId,
        this.thumbnail = thumbnail,
        this.position = position,
        this.channelName = channelName,
        this.description = description
        this.type = type
    }
}

module.exports = youtubeVideo;