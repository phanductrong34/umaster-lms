const router = require('express').Router();
const axios  = require('axios');
const {response} = require('express');
const youtubeVideo = require('../models/youtubeVideo');


router.get("/playlist/:id/list", async (req, res) => {
    try{
        //params cua queryString
        let params = {
            part: "snippet",
            playlistId: req.params.id,
            key: process.env.YOUTUBEKEY,
            maxResults: 50
        }
        // Sinh ra queryString
        const esc = encodeURIComponent;
        let queryString = Object.keys(params).map(k => esc(k) + '=' + esc(params[k]) ).join('&');
        let URI = constant.ytURI + "playlistItems?" + queryString;

        // request lay playlist
        const response = await axios.get(URI)
        const playlist = response.data.items.map(vid => {
            return new youtubeVideo(
                vid.snippet.title,
                vid.id,
                vid.snippet.resourceId.videoId,
                vid.snippet.videoOwnerChannelId,
                vid.snippet.thumbnails,
                vid.snippet.position,
                vid.snippet.videoOwnerChannelTitle,
                vid.snippet.description,
                "vid"
            )
        })
        res.set('Cache-Control', 'public, max-age=300, s-maxage=600')
        res.json({
            success: true,
            data: playlist,
        }) 
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })

    }
})


module.exports = router;
