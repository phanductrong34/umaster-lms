const cloudinary = require('cloudinary').v2;
const multer = require('multer'); //parsing formdata from post
const upload = multer()
const streamifier = require('streamifier');

const presetUpload = (req,preset) => {
    let result;
    switch(preset){
        case "preset1": //thumbnail bài tập - ảnh
            result = {  
                folder: "homework/thumb",
                width: 420,           // nén kích thước giữ tỉ lệ
                fetch_format: "auto",  // tự chuyển định dạng
                crop: "scale"
            };
            break;
        case "preset2": //File bài tập - nén kiểu rar,zip
            result = {
                public_id: req.body.fileName,
                folder: "homework/file",
                resource_type: "auto",                
            }
            break;

    }
    return result
}

// hàm đẩy từ buffer của multer ra stream upload lên cloudinary
const streamUpload = (req, preset) => {
    return new Promise((resolve, reject) => {
        let stream = cloudinary.uploader.upload_stream(
            presetUpload(req,preset),
            (error,result)=> {
                if(result){
                    resolve(result);
                }else{
                    reject(error);
                }
            }
        );

        streamifier.createReadStream(req.file.buffer).pipe(stream);
    })
};

const streamUploadMutiple = async (req,folder) => {
    let resultFiles = []
    await Promise.all(req.files.forEach( (file) => {
        return new Promise((resolve, reject) => {
            let stream = cloudinary.uploader.upload_stream(
                presetUpload(preset,config),
                (error,result)=> {
                    if(result){
                        resultFiles.push({
                            url: result.secure_url,
                            public_id: result.public_id,
                            file_type: result.metadata,
                        })
                    }else{
                        reject(error);
                    }
                }
            );
    
            streamifier.createReadStream(file.buffer).pipe(stream); 
        }) 
    }));
    return resultFiles;
};

module.exports = {streamUpload,streamUploadMutiple};