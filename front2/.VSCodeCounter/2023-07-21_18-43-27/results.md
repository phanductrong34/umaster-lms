# Summary

Date : 2023-07-21 18:43:27

Directory d:\\CODE\\umaster-lms\\front2

Total : 200 files,  36308 codes, 1420 comments, 2491 blanks, all 40219 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 13 | 16,484 | 10 | 4 | 16,498 |
| Vue | 93 | 15,722 | 873 | 1,871 | 18,466 |
| JavaScript | 59 | 3,767 | 522 | 539 | 4,828 |
| SCSS | 8 | 141 | 4 | 29 | 174 |
| XML | 22 | 83 | 1 | 2 | 86 |
| Markdown | 1 | 79 | 0 | 35 | 114 |
| HTML | 1 | 20 | 7 | 7 | 34 |
| Docker | 1 | 10 | 3 | 3 | 16 |
| Ignore | 1 | 1 | 0 | 0 | 1 |
| Log | 1 | 1 | 0 | 1 | 2 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 200 | 36,308 | 1,420 | 2,491 | 40,219 |
| . (Files) | 9 | 14,975 | 19 | 49 | 15,043 |
| functions | 4 | 1,718 | 15 | 18 | 1,751 |
| public | 13 | 79 | 8 | 8 | 95 |
| public (Files) | 1 | 20 | 7 | 7 | 34 |
| public\\assets | 12 | 59 | 1 | 1 | 61 |
| public\\assets\\anim | 7 | 7 | 0 | 0 | 7 |
| public\\assets\\png | 5 | 52 | 1 | 1 | 54 |
| src | 174 | 19,536 | 1,378 | 2,416 | 23,330 |
| src (Files) | 3 | 759 | 35 | 102 | 896 |
| src\\assets | 18 | 32 | 0 | 1 | 33 |
| src\\assets (Files) | 1 | 15 | 0 | 1 | 16 |
| src\\assets\\anim | 1 | 1 | 0 | 0 | 1 |
| src\\assets\\png | 16 | 16 | 0 | 0 | 16 |
| src\\components | 60 | 8,850 | 636 | 1,105 | 10,591 |
| src\\components (Files) | 4 | 1,638 | 54 | 189 | 1,881 |
| src\\components\\Auth | 1 | 213 | 6 | 34 | 253 |
| src\\components\\Base | 4 | 301 | 13 | 32 | 346 |
| src\\components\\Classes | 6 | 915 | 88 | 147 | 1,150 |
| src\\components\\Comments | 1 | 208 | 7 | 24 | 239 |
| src\\components\\Courses | 1 | 152 | 10 | 14 | 176 |
| src\\components\\EditorAdmin | 7 | 490 | 284 | 87 | 861 |
| src\\components\\EditorUser | 7 | 332 | 38 | 65 | 435 |
| src\\components\\Guest | 3 | 408 | 13 | 41 | 462 |
| src\\components\\Labo | 12 | 2,462 | 58 | 298 | 2,818 |
| src\\components\\Lessons | 4 | 315 | 10 | 30 | 355 |
| src\\components\\Resource | 1 | 74 | 0 | 13 | 87 |
| src\\components\\Students | 5 | 759 | 35 | 88 | 882 |
| src\\components\\Teachers | 1 | 99 | 1 | 6 | 106 |
| src\\components\\Work | 3 | 484 | 19 | 37 | 540 |
| src\\composable | 26 | 551 | 42 | 136 | 729 |
| src\\firebase | 1 | 30 | 5 | 8 | 43 |
| src\\router | 1 | 370 | 6 | 28 | 404 |
| src\\store | 11 | 1,787 | 104 | 173 | 2,064 |
| src\\store (Files) | 1 | 28 | 0 | 3 | 31 |
| src\\store\\modules | 10 | 1,759 | 104 | 170 | 2,033 |
| src\\styles | 8 | 141 | 4 | 29 | 174 |
| src\\views | 46 | 7,016 | 546 | 834 | 8,396 |
| src\\views\\Admin | 16 | 2,313 | 378 | 289 | 2,980 |
| src\\views\\Admin (Files) | 5 | 391 | 35 | 71 | 497 |
| src\\views\\Admin\\CoursesCollection | 6 | 1,078 | 298 | 121 | 1,497 |
| src\\views\\Admin\\ResourceCollection | 3 | 535 | 13 | 53 | 601 |
| src\\views\\Admin\\TeachersForm | 2 | 309 | 32 | 44 | 385 |
| src\\views\\Guest | 9 | 1,349 | 19 | 139 | 1,507 |
| src\\views\\Login | 2 | 372 | 13 | 34 | 419 |
| src\\views\\NotFound | 1 | 73 | 0 | 5 | 78 |
| src\\views\\User | 18 | 2,909 | 136 | 367 | 3,412 |
| src\\views\\User (Files) | 7 | 1,221 | 87 | 183 | 1,491 |
| src\\views\\User\\ClassroomCollection | 1 | 121 | 2 | 13 | 136 |
| src\\views\\User\\DashboardCollection | 8 | 1,333 | 39 | 149 | 1,521 |
| src\\views\\User\\LaboCollection | 1 | 73 | 4 | 7 | 84 |
| src\\views\\User\\ResourceUserCollection | 1 | 161 | 4 | 15 | 180 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)