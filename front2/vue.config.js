/*
INJECT DATA VÀO TỪ ĐẦU CÁI FILE SCSS MAIN
*/



module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/styles/main.scss";'
          //cũng đc chuyển thành html sẽ nhận đc tác dụng của cái file main ấy
      }
    }
  },

  assetsDir: 'assets',

  devServer: {
    port: 8081,
    host: '0.0.0.0'
  },

  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false
    }
  },

  transpileDependencies: [
    'quasar'
  ]
};
