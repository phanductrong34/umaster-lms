# resource-page-v2

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Test Before DEPLOY
firebase serve
(có thể dùng nvm đổi về node cao hơn)

### Deploy to firebase hosting (after build ở front)
firebase deploy --only hosting:umaster-academy-pdt

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

scr
-assets
-----png
-------picture.png
-----fonts
-------Arial.ttf
-style
----main.scss
-components
----Home.vue
-App.vue 
vue.config.js


Thêm và sử dụng static assets: 
- thêm vào thư mục src/assets các kiểu font, ảnh, icon ...
- ở các file .vue trong folder component hay view sử dụng relative path như trong thư mục scr: 
    VD nếu lấy từ App.vue thì chỉ cần ./assets/png đc rồi
    nhưng nếu lấy ở Home.vue thì ../assets/png
-tuy nhiên nếu có sử dụng sass và vue.config.js để tạo global css thì để dùng relative path ta thêm '~@/assets/png' ==> ~@ là /scr
    



File vue.config.js laf optional thêm vào để điều chỉnh các mặc định webpack đã đc ẩn đi
https://cli.vuejs.org/config  vào đây để xem thêm

VD để add sass global vào vue 
https://css-tricks.com/how-to-import-a-sass-file-into-every-vue-component-in-an-app/
tạo folder chứa toàn bộ file scss, co thể import hết vào 1 cái main.scss
viết config mới để nó nhận file scss tổng ấy , sau đó vào các component vue sử dụng variable sass ầm ầm thôi



dùng node ver 14.15.7 để ko bị lỗi các package
Hoặc nếu muốn cài nhiều phiên bản node và đổi lại cho nhau thì xem link này:
https://www.freecodecamp.org/news/nvm-for-windows-how-to-download-and-install-node-version-manager-in-windows-10/
cách lệnh sau (chạy bằng powershell hoặc cmd admin mode)
nvm list : để xem version có
nvm install lts : install bản ổn định mới
nvm instal 14.15.0 : install bản cụ thể
nvm use 14.15.0 : chuyển node



hoặc xài yarn add từ đầu và các lệnh của yarn để nó fix mọi thứ
dùng lại node: npm install --legacy-peer-deps để ko còn bị lỗi vue router nữa



## Cách xài EditorJS
- viết ra 1 file js chứa tất cả các class tự định nghĩa
- 1 class sẽ cần tối thiểu 1 hàm static toolbox để hiển thị icon để kích hoạt, 1 constructor, data để load data từ cục JSON vào
- 1 hàm render trả về HTML (đã đc nạp data vào) và 1 hàm save() để trả về dữ liệu sau đc lưu vào biến data trong JSON
- 1 hàm validate để xử lý dữ liệu lấy đc trước khi chạy save(), để không lưu những dữ liệu lỗi, rỗng nữa

- constructor cũng là cách để tạo biến rỗng sau dùng ở nhiều hàm qua lại của 1 class bằng biến this


RENDER: 
- Load data lúc khởi tạo truyền vào từ khối Json
- return khối HTML để render
- Gắn sự kiện cho các thành phần ở đây luôn
- Hàm cho các sự kiện gọi là helpẻr để ở ngoài

SAVE(blockContent)
- Nhận vào chính cái cục html ở RENDER
- Lấy data từ đó ra, vào return ra 1 object chứa dữ liệu, cái sẽ đc lưu vào biến ở JSON


## DOCKER

FROM node:14.15.0 as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build


FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 9000
CMD ["nginx", "-g", "daemon off;"]


