import getCollectionFilter from "@/composable/getCollectionFilter";
import {
  FieldValue, projectFirestore, projectFunctions, timestamp
} from "@/firebase/config";

import removeDocsFilter from "@/composable/removeDocsFilter";
import setDoc from "@/composable/setDoc";
import updateDoc from "@/composable/updateDoc";
import useCollection from "@/composable/useCollection";
import { useToast } from "vue-toastification";
import removeDoc from "@/composable/removeDoc";
 
const toast = useToast();

//hàm rải data của doc từ onSnapshot và thêm trường id nữa
const transformDoc = (document) => {
  return { ...document.doc.data(), id: document.doc.id };
};

const moduleAdmin = {
        namespaced: true,
        state: {
          classWorks: {}, //"mt1.1":[work1,work2,..],
          classStudents: {}, //"mt1.1": [student1, student2],
          activeStudentID: null,
          activeClassID: null,
        },
        getters: {
          getClassWorks: (state) => (classID) => {
            if (state.classWorks[classID]) {
              return state.classWorks[classID];
            } else {
              return null;
            }
          },
          getStudentsFromClass: (state) => (classID) => {
            if (!state.classStudents[classID]) {
              // nhận null(chưa load), nhận [] (laod rồi nhưng rỗng), [student, student2] nếu có
              return null;
            } else if (state.classStudents[classID].length == 0) {
              return [];
            } else {
              return state.classStudents[classID];
            }
          },
          getActiveStudentID(state) {
            return state.activeStudentID;
          },
        },
        mutations: {
          setActiveStudentID(state, newID) {
            state.activeStudentID = newID;
          },
        },
        actions: {
          resetAdmin({ state }) {
            state.classWorks = {};
            state.classStudents = {};
            state.activeStudentID = null;
            state.activeClassID = null;
          },
          async loadClassWorks({ state }, { classID }) {
            const { dataArray: classWorks, error, load } = getCollectionFilter();
            await load("works", "classID", classID);
            if (error.value) {
              // nếu có lỗi
              toast.error("Load Class Works Failed");
              console.log(error.value);
              return null;
            } else if (classWorks.value.length == 0) {
              // nếu không lỗi nhưng ko có bài
              state.classWorks[classID] = [];
              return 0;
            } else {
              // không lỗi và có bài
              state.classWorks[classID] = classWorks.value;
              return classWorks.value;
            }
          },
      
          //fetch students thuộc trong 1 lớp và lưu lại owr dajgn array
          async loadStudentsClass({ state }, { classID }) {
            const { dataArray: students, error, load } = getCollectionFilter();
            await load("students", "classID", classID);
            if (!error.value) {
              //nếu fetch không lỗi
              if (students.value.length == 0) {
                // ko lỗi những không có data
                state.classStudents[classID] = [];
                return 0;
              } else {
                state.classStudents[classID] = students.value;
                return students.value;
              }
            }
          },
      
          async deleteWork({ state }, { workID, studentID, classID }) {
            /////////////xóa ở collection work
            const { remove, error: err1 } = removeDoc("works");
            await remove(workID);
            if (err1.value) {
              console.log(err1.value);
              toast.error("Delete homework failed");
              return null;
            }
      
            //xóa ở student works array bằng cách update lại field, fiter ra
            const { update, error: err2 } = updateDoc("students");
            await update(studentID, {
              works: FieldValue.arrayRemove(workID),
            });
            if (err1.value) {
              console.log(err2.value);
              toast.error("Delete homework failed");
              return null;
            }
      
            // thành công thì xóa ở state offline array worklist
            state.classWorks = state.classWorks[classID].filter(
              (work) => work.id !== workID
            );
            //xóa offline ở trường works trong student
            const index = state.classStudents[classID].findIndex(
              (student) => student.id == studentID
            );
            state.classStudents[classID][index].works = state.classStudents[classID][
              index
            ].works.filter((id) => id !== workID);
            toast.clear();
            toast.success(`Delete homework successfully!!`);
          },
      
          // thêm học viên mới ở auth, lấy id ở auth trả về thêm ở firestore và offline và classStudent
          async addNewStudent(
            { state },
            { newStudents, activeClassID, activeCourseID, defaultPass }
          ) {
            //input là array
            const { error : err1, set: setStudentDoc } = setDoc("students");
            const { error: err2, addDoc: createStudentRecord } = useCollection("studentRecord");
            const createUser = projectFunctions.httpsCallable("createUser");
            const deleteUser = projectFunctions.httpsCallable("deleteUser");

            toast.info(`Adding new students to class ${activeClassID}...`);
            //format lại
            const newStudentsArray = newStudents.map((student, index) => {
              return {
                ...student,
                classID: activeClassID,
                courseID: activeCourseID,
                fullname: "",
                works: [],
                phone: "",
                avaRef: `ava/ava-(${Math.ceil(Math.random() * 50)}).svg`,
                //avaURL: ,
                createdAt: timestamp(),
                isNewUser: true,
              };
            });
            // loop qua array mới tạo tk cho user và lấy id tạo mới user và studentRecord lên firestore, rồi set vào cuối classStudents[activeClassID] offline
            newStudentsArray.forEach(async (newStudent) => {
              const resCreate = await createUser({
                email: newStudent.email,
                password: defaultPass,
              });
              if (resCreate.data) {
                    const uid = resCreate.data.uid;
                    const newStudentRecord = {
                      studentID: uid,
                      classID: activeClassID,
                      courseID: activeCourseID,
                      createdAt: timestamp(),
                      payment: true,
                      activeLessonID: "nHsv3b0xOBfGVv5ZZjXt",    //lessonID bài 1
                      activeSec: 0,
                      activeVideoID: "SjzoQSa_I78",  //Video đầu bài 1
                      note:[],
                      lessonCompleted: {}
                    }
                    //Tạo doc trong bảng user và doc trong bảng studentRecord luôn
                    await Promise.all([
                      setStudentDoc(uid, newStudent),
                      createStudentRecord(newStudentRecord)
                    ]);
                    
                    if(err1.value || err2.value){
                      deleteUser({id: uid});
                      toast.error(`Tạo tài khoản cho học viên ${newStudent.nickname} thất bại`);
                      return;
                    }

                    state.classStudents[activeClassID].push({ ...newStudent, id: uid });
                    toast.clear();
                    toast.success(
                      `Thêm tài khoản cho ${newStudent.nickname} thành công !`
                    );
              } else {
                toast.clear();
                toast.error(`Email ${newStudent.email} đã tồn tại ở một lớp khác`);
              }
            });
          },
      
          /* tìm work ở firestore và xóa hết trước
          xóa học viên, xóa cả tk ở auth, 
          xóa doc ở firestore cả student 
          xóa offline cả classWorks,classStudents, 
          đổi lại activeStudent vè đầu*/
          async deleteStudent({ state }, { studentID, classID }) {
            toast.info("Delete student and works...");
            const { error: errWorks, remove: removeWorks } = removeDocsFilter();
            const { error: errStudent, remove: removeStudent } =
              removeDoc("students");
            const deleteUser = projectFunctions.httpsCallable("deleteUser");
      
            removeWorks("works", "studentID", studentID);
            removeStudent(studentID);
            await deleteUser({ uid: studentID });
            if (!errWorks.value && !errStudent.value) {
              state.classWorks[classID] = state.classWorks[classID].filter(
                (work) => work.studentID !== studentID
              );
              state.classStudents[classID] = state.classStudents[classID].filter(
                (student) => student.id !== studentID
              );
              toast.clear();
              toast.success("Delete student successfully!");
            } else {
              toast.clear();
              toast.error("Delete student failed!");
              console.log(errWorks.value, errStudent.value);
            }
          },
      
          //update ở fireStore và offline classStudent + classWorks là xong
          async updateStudent({ state }, { studentID, classID, newData }) {
            const { error: errUpdate, update } = updateDoc("students");
            await update(studentID, newData);
            if (!errUpdate.value) {
              //update thông tin ở student đang đổi
              const idx1 = state.classStudents[classID].findIndex(
                (stu) => stu.id == studentID
              );
              state.classStudents[classID][idx1] = {
                ...state.classStudents[classID][idx1],
                fullname: newData.fullname,
                nickname: newData.nickname,
                phone: newData.phone,
              };
              //update thông tin ở các work trùng với student đang update
              state.classWorks[classID].forEach((work, index) => {
                if (work.studentID == studentID) {
                  state.classWorks[classID][index].fullname = newData.fullname;
                  state.classWorks[classID][index].nickname = newData.nickname;
                  state.classWorks[classID][index].phone = newData.phone;
                }
              });
      
              toast.clear();
              toast.success("Update student successfully!");
            } else {
              toast.clear();
              toast.error("Update student failed");
            }
          },
      
          async addNewLesson({ state }, { newLesson }) {
            if (newLesson.tags.length == 0) {
              toast.error("You must have at least 1 tag");
              return false;
            } else {
              //check có tồn tại trong courseID ấy có number ấy chưa
              const res = await projectFirestore
                .collection("lessons")
                .where("courseID", "==", newLesson.courseID)
                .where("number", "==", newLesson.number)
                .get();
      
              if (res.size) {
                // nếu tồn tại lesson có number ấy rồi
                toast.error(
                  `Lesson ${newLesson.number} is already exists. Choose another one`
                );
                return false;
              } else {
                const { error: errAdd, addDoc } = useCollection("lessons");
                await addDoc(newLesson);
                if (!errAdd.value) {
                  toast.clear();
                  toast.success(`Create new lesson successfully!`);
      
                  return true;
                } else {
                  toast.error("Create new lesson failed");
                  return false;
                }
              }
            }
          },
      
          async updateLesson({ state }, { lessonID, updatedLesson, lessonNumber }) {
            const { error: errUpdate, update } = updateDoc("lessons");
            await update(lessonID, updatedLesson);
            if (!errUpdate.value) {
              toast.clear();
              toast.success(`Cập nhật bài giảng ${lessonNumber} thành công!`);
              return true;
            } else {
              toast.error("cập nhật bài giảng thất bại");
              return false;
            }
          },
      
          async deleteLesson({ state }, { lessonID, lessonNumber }) {
            const { error: errRemove, remove } = removeDoc("lessons");
            await remove(lessonID);
            if (!errRemove.value) {
              toast.clear();
              toast.success(`Delete lesson ${lessonNumber} succeessfully`);
              return true;
            } else {
              toast.error(`Delete lesson ${lessonNumber} failed`);
              return false;
            }
          },
      
          async addNewCourse({ state }, { courseID, newCourse }) {
            //check có tồn tại courseID ấy chưa
            const res = await projectFirestore
              .collection("courses")
              .doc(courseID)
              .get();
            if (res.exists) {
              // nếu tồn tại
              toast.error(`${courseID} is already exists. Choose another one`);
              return false;
            } else {
              const { error: errSet, set } = setDoc("courses");
              await set(courseID, newCourse);
              if (!errSet.value) {
                toast.clear();
                toast.success(`Create new course successfully!`);
              } else {
                toast.error(`Create new course failed`);
                return false;
              }
            }
          },
      
          async updateCourse({ state }, { courseID, updatedCourse }) {
            const { error: errUpdate, update } = updateDoc("courses");
            await update(courseID, updatedCourse);
            if (!errUpdate.value) {
              toast.clear();
              toast.success(`Update course ${courseID} successfully!`);
              return true;
            } else {
              toast.error("Update course failed - 1");
            }
          },
      
          async deleteCourse({ state }, { courseID }) {
            // xóa online trước theo thứ tự: work -> student -> class -> lesson -> course mà có courseID trùng
      
            //batch delete works
            const { error: errWorks, remove: removeWorks } = removeDocsFilter();
            removeWorks("works", "courseID", courseID);
            if (errWorks.value) {
              toast.error("Delete course failed! - 2");
              return false;
            }
      
            //load tất cả student có course id ấy để xóa tài khoảng trong auth
            const deleteMultiUser = projectFunctions.httpsCallable("deleteMultiUser");
            const {
              dataArray: filterStudents,
              error: errGetStudent,
              load: loadStudent,
            } = getCollectionFilter();
            await loadStudent("students", "courseID", courseID);
            if (filterStudents.value.length) {
              const ids = filterStudents.value.map((student) => student.id);
              deleteMultiUser({ ids: ids });
            }
      
            const { error: errRemove, remove: removeDocs } = removeDocsFilter();
            //batch delete students
            removeDocs("students", "courseID", courseID);
            if (errRemove.value) {
              toast.error("Delete course failed! - 3");
              return false;
            }
      
            // batch delete class
            // lấy ds các class có courseID ấy về để xóa offline trong object classWorks và classStudents
            const {
              dataArray: classes,
              error: errGetClasses,
              load: loadClasses,
            } = getCollectionFilter();
            await loadClasses("classes", "courseID", courseID);
            classes.value.forEach((classi) => {
              delete state.classStudents[classi.id];
              delete state.classWorks[classi.id];
            });
      
            removeDocs("classes", "courseID", courseID);
            if (errRemove.value) {
              toast.error("Delete course failed! - 4");
              return false;
            }
            //batch delete lesson
            removeDocs("lessons", "courseID", courseID);
            if (errWorks.value) {
              toast.error("Delete course failed! - 5");
              return false;
            }
      
            //delete course
            const { remove: removeCourse, error: errCourse } = removeDoc("courses");
            await removeCourse(courseID);
            if (errCourse.value) {
              toast.error("Delete course failed! - 6");
              return false;
            } else {
              // ko cần xóa offline vì mất lớp rồi thì cũng khỏi hiện
              toast.clear();
              toast.success("Delete course successfully!");
              return true;
            }
          },
      
          async createFile({ state }, { newFile }) {
            if (newFile.from.length == 0 || newFile.type.length == 0) {
              toast.error("You must choose both from and type");
              return false;
            }
            const { error: errCreate, addDoc } = useCollection("files");
            await addDoc(newFile);
            if (!errCreate.value) {
              toast.clear();
              toast.success(`Create new ${newFile.folder} successfully!`);
              return true;
            } else {
              toast.error(`Create ${newfile.folder} failed`);
              return false;
            }
          },
      
          async updateFile({ state }, { fileID, updatedFile, folder }) {
            const { error: errUpdate, update } = updateDoc("files");
            await update(fileID, updatedFile);
            if (!errUpdate.value) {
              toast.clear();
              toast.success(`Update ${folder} successfully!`);
              return true;
            } else {
              toast.error(`Update ${folder} failed`);
              return false;
            }
          },
      
          async deleteFile({ state }, { fileID, folder }) {
            const { remove, error } = removeDoc("files");
            await remove(fileID);
            if (!error.value) {
              toast.clear();
              toast.success(`Delete ${folder} successfully!`);
              return true;
            } else {
              toast.error(`Delete ${folder} failed`);
              return false;
            }
          },
        },
};
export default moduleAdmin