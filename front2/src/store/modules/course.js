import getDoc from "@/composable/getDoc";

import { useToast } from "vue-toastification";

const toast = useToast();

const course = {
        namespaced: true,
        state: {
          currentCourse: null,
        },
        getters: {
          getCurrentCourse(state) {
            return state.currentCourse;
          },
        },
        mutations: {
          setCurrentCourse(state, data) {
            state.currentCourse = data;
          },
          resetCourse(state) {
            state.currentCourse = null;
          },
        },
        actions: {
          async firstLoadCourse({ rootGetters, commit }) {
            const courseID = rootGetters["user/getCourseID"];
            const { data, error, load } = getDoc("courses");
            if (courseID) {
              await load(courseID);
              commit("setCurrentCourse", data.value);
            }
          },
          resetCourse({ commit }) {
            commit("resetCourse");
          },
        },
};
export default course

      