
import { useToast } from "vue-toastification";
import getCollectionFilter from "../../composable/getCollectionFilter";
import {projectFunctions, projectFirestore,FieldValue} from "../../firebase/config";
import HTTP from "../../composable/axios";
import { useStorage } from '@vueuse/core'
import updateDoc from "../../composable/updateDoc";

const toast = useToast();

const devClient = "http://localhost:8081/";
const prodClient = "https://umaster-admin.web.app";

const player = {
    namespaced: true,
    state: {
        publishs: {}, //{pubID: {}}
        activePublish: {},
        activeOrder:{},
        activePaymentData:{},
        orderData: useStorage('orderData',{}),
    },
    getters: {
    },
    actions:{
        async checkCoupon ({state}, {code,courseId}){
            console.log(state.orderData);
            const {dataArray : codes, error, load} = getCollectionFilter();
            await load('promotions','code',code);
            console.log(codes.value);
            if(!error.value){
                if(codes.value.length > 0){
                    let code = codes.value[0];
                    if(code.courseID && code.courseID != courseId) return false;
                    let isLeft = Number(code.history.length) < Number(code.quantity);
                    let isNotExpired = new Date(code.expiredDate) > new Date();
                    if(isLeft && isNotExpired){
                        return code;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        },

        async createPaymentUrl ({state},{codeID, price, info,orderData}) {

            const amount = Number(price);
            const orderType = "topup";
            const return_route = "user/guestCourses/detail/vnpay_return";
            const orderInfo = info;
            const returnUrl = `${devClient}${return_route}`; // thay lại bằng link trang web client
            const bankCode = "";  //VNPAYQR-VNBANK-INTCARD-Trống
            const language = "vn";

            //Lưu lại order vào local storage
            state.orderData = orderData;
            const res = await HTTP.post('api/order/create_payment_url', {amount, orderType, orderInfo, returnUrl, bankCode, language} )
            if(res.success){
                
              window.open(res.data.vnpUrl, '_self')
              return res.data.vnpUrl;
            }
          },
      
          //lấy query string cura activePaymentData và query lên back end
          async getVnpayReturn({state},{query}){
            let data = state.orderData;
            console.log(data);
            console.log(state.orderData.value, state.orderData);
            if(query){
              const res = await HTTP.get('api/order/vnpay_return',{params: query})
              console.log(res);
              if(res.success){
                let code = res.data.code;
                // Thanh toàn thành công
                if(code == "00"){
                    const createStudentRecordOn = projectFunctions.httpsCallable('createStudentRecordOn');
                    let res2 = await createStudentRecordOn({
                        classID: data.classID,
                        courseID: data.courseID,
                        studentID: data.studentID,
                        studentName: data.studentName,
                        courseName: data.courseName,
                        classFormat : data.classFormat,
                        avaRef: data.avaRef
                    });
                    if(res2.data.success){
                        const {error: err3, update} = updateDoc('promotions');
                        let res3 = await update(data.promotionCodeID,{history: FieldValue.arrayUnion({
                            studentID: data.studentID,
                            name: data.studentName,
                            email: data.email,
                            phone: data.phone,
                            createdAt: new Date(),
                            useForCourse: data.courseID
                        })});
                        if(!err3.value){
                            toast.success("Thanh toán thành công");
                            state.orderData.value = {};
                            return "00"
                        }else{
                            toast.error("Thanh toán không thành công");
                            state.orderData.value = {};
                            return false
                        }
                    }else{
                        toast.error("Thanh toán không thành công");
                        state.orderData.value = {};
                        return false
                    }
                }
                return code;
              }else{
                state.orderData.value = {};
                return false;
              }
              
            }
            state.orderData.value = {};
            return false;
          },  
    }
};
export default player