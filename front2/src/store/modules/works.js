import getCollectionFilter from "@/composable/getCollectionFilter";
import getCollectionFilter2 from "@/composable/getCollectionFilter2";
import getCollectionLimit from "@/composable/getCollectionFilterByPage";
import {
  FieldValue, projectFirestore,
  timestamp
} from "@/firebase/config";

import updateDoc from "@/composable/updateDoc";
import useCollection from "@/composable/useCollection";
import { useToast } from "vue-toastification";
import removeDoc from "@/composable/removeDoc";

const toast = useToast();

const works = {
        namespaced: true,
        state: {
          workList: [],
          workGallery: {},
          endDoc: null,
          isLoadAlls: {}, // lessonNumber: true
        },
        getters: {
          getWorks: (state) => (number) => {
            // trả về array works của 1 lesson trong mảng tất cả bài của học viên ấy
            if (state.workList.length == 0) return null;
            else {
              const curWorks = state.workList.filter(
                (work) => work.lessonNumber == number
              );
              if (curWorks.length > 0) return curWorks;
              else return null;
            }
          },
          getWorkList(state) {
            return state.workList;
          },
          // getWorkCount(state){
          //   return state.list.length;
          // },
          getWorkGalleryFromLesson: (state) => (lessonNumber) => {
            return state.workGallery[lessonNumber];
          },
          isLessonLoadAll: (state) => (lessonNumber) => {
            return state.isLoadAlls[lessonNumber];
          }
        },
        mutations: {
          pushWork(state, work) {
            state.workList.push(work);
          },
          setWorks(state, works) {
            state.workList = works;
          },
          updateWork(state, newWork) {
            const index = state.workList.findIndex((work) => work.id == newWork.id);
            if (index > -1) {
              state.workList[index] = newWork;
            }
          },
          resetWorks(state) {
            state.workList = [];
          },
        },
        actions: {
          resetWorks({ commit }) {
            commit("resetWorks");
          },
          async firstLoadWorks({ rootGetters, commit }) {
            commit("resetWorks");
            const studentID = rootGetters["user/getCurrentUser"].uid;
            const {
              dataArray: works,
              error,
              load: loadWorks,
            } = getCollectionFilter();
            await loadWorks("works", "studentID", studentID);
      
            if (works.value.length) {
              commit("setWorks", works.value);
            }
          },
          //payload = {inputURL, number}
          async uploadWork(
            { commit, rootGetters },
            {
              videoUrl,
              thumbnailUrl,
              workSize,
              workName,
              workDuration,
              publicId,
              number,
              downloadUrl,
            }
          ) {
            const newWork = {
              lessonNumber: number,
              videoUrl,
              thumbnailUrl,
              workSize,
              workDuration,
              workName,
              publicId,
              downloadUrl,
              studentNickname: rootGetters["user/getUserData"].nickname,
              studentFullName: rootGetters["user/getUserData"].fullname,
              avaRef: rootGetters["user/getUserData"].avaRef,
              studentID: rootGetters["user/getCurrentUser"].uid,
              classID: rootGetters["user/getClassID"],
              courseID: rootGetters["user/getCourseID"],
              createdAt: timestamp(),
              score: 0,
            };
            const { error: err1, addDoc } = useCollection("works");
            const res = await addDoc(newWork);
            if (!err1.value) {
              //Đợi để lấy ID sau đó
              commit("pushWork", { ...newWork, id: res.id, createdAt: "Just now" });
              // thêm id này vào works curea học sinh
              await projectFirestore
                .collection("students")
                .doc(newWork.studentID)
                .update({
                  works: FieldValue.arrayUnion(res.id),
                });
      
              // thêm work mới này vào studentWorks module để cập nhật ở trang classroom
              // commit('studentWorks/pushWork',{...newWork, id: res.id, createdAt: 'Just now'},{root : true})
      
              //toast thành công ở đây
              toast.clear();
              toast.success("Upload bài tập thành công");
              return newWork;
            } else {
              toast.error("Upload bài tập thất bại");
              return null;
            }
          },
      
          async deleteWork({ state, rootGetters, commit }, { workID, studentID }) {
            //xóa ở collection work
            const { remove, error: err1 } = removeDoc("works");
            await remove(workID);
            if (err1.value) {
              console.log(err1.value);
              toast.error("Xóa bài tập thất bại");
              return false;
            }
      
            //xóa ở student works array bằng cách update lại field, fiter ra
            const { update, error: err2 } = updateDoc("students");
            const studentId = rootGetters;
            await update(studentID, {
              works: FieldValue.arrayRemove(workID),
            });
            if (err1.value) {
              console.log(err2.value);
              toast.error("Xoá bài tập thất bại");
              return false;
            }
      
            // thành công thì xóa ở state offline array worklist
            state.workList = state.workList.filter((work) => work.id !== workID);
      
            // và xóa  ở trường studentWorks + studentsList trong module studentWorks
            const data = { workID, studentID }; // phải làm thế này vì commit ở module khác chỉ cho phép 1 biến đc truyền
            // commit('studentWorks/deleteWork',data, {root: true});
            toast.clear();
            toast.success(`Xoá bài tập thành công !`);
            return true;
            //
          },
      
          //Load work trùng courseID và lesson Number về (bài số bao nhiêu trong cùng khoá)
          async loadWorksNumber({ state }, { courseID, lessonNumber }) {
            if (state.workGallery[lessonNumber]) {
              return state.workGallery[lessonNumber];
            }
            console.log("loaddddddd");
            
            const {dataArray : workArray ,endDoc, isLoadAll, error, load2, next2} = getCollectionLimit();
            await load2("works",6, "courseID", courseID, "lessonNumber", lessonNumber);
            if (!error.value && workArray.value.length > 0) {
              state.workGallery[lessonNumber] = workArray.value;
              state.endDoc = endDoc.value;
              state.isLoadAlls[lessonNumber] = isLoadAll.value;
              return true
            }
      
            return false;
          },

          async nextWorkPage({state}, {courseID, lessonNumber}){
            if(state.isLoadAlls[lessonNumber]) return false;

            const {dataArray : workArray ,endDoc,isLoadAll, error, load2, next2} = getCollectionLimit();
            console.log("nextttttt", state.endDoc);
            await next2("works",6, state.endDoc, "courseID", courseID, "lessonNumber", lessonNumber)
            if(!error.value && workArray.value.length > 0){
              workArray.value.forEach((work) => {
                state.workGallery[lessonNumber].push(work)
              })
              state.endDoc = endDoc.value
              state.isLoadAlls[lessonNumber] = isLoadAll.value;
              return true
            }
            return false
          }
        },
};

export default works;
