import getDoc from "@/composable/getDoc";
import { useToast } from "vue-toastification";
const toast = useToast();

const classes = {
        namespaced: true,
        state: {
          currentClass: null,
          videoRecords: null, // 1: youtubeURL của video quay
        },
        getters: {
          getCurrentClass(state) {
            return state.currentClass;
          },
          getVideoRecords(state) {
            return state.videoRecords;
          },
        },
        mutations: {
          setCurrentClass(state, data) {
            state.currentClass = data;
          },
          resetClass(state) {
            state.currentClass = null;
            state.videoRecords = null;
          },
        },
        actions: {
          async firstLoadClass({ state, rootGetters, commit }) {
            const classID = rootGetters["user/getClassID"];
            const { data, error, load } = getDoc("classes");
            if (classID) {
              await load(classID);
              commit("setCurrentClass", data.value);
              if (data.value.videoRecords) {
                state.videoRecords = data.value.videoRecords;
              }
            }
          },
          resetClass({ commit }) {
            commit("resetClass");
          },
        },
};
export default classes