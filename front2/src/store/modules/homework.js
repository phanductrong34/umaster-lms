import getDoc from "@/composable/getDoc";

import { useToast } from "vue-toastification";

const toast = useToast();

const homework = {
        namespaced: true,
        state: {
          homeworks: {}, //lessonid: object
        },
        getters: {},
        actions: {
          async loadHomework({ state }, { lessonID }) {
            if (state.homework[lessonID]) {
              return state.homeworks[lessonID];
            }
            const { data, error, load } = getDoc("lessons");
            await load(lessonID);
            if (!error.value) {
              state.homeworks[lessonID] = data.value.editorData;
              return state.homeworks[lessonID];
            }
          },
        },
};

export default homework