import getCollectionFilter from "@/composable/getCollectionFilter";
import getDoc from "@/composable/getDoc";
import {
  projectFirestore,
  timestamp
} from "@/firebase/config";

import updateDoc from "@/composable/updateDoc";
import useCollection from "@/composable/useCollection";
import _ from "lodash";
import { useToast } from "vue-toastification";
import HTTP from "@/composable/axios";
import removeDoc from "@/composable/removeDoc";

const toast = useToast();

//hàm rải data của doc từ onSnapshot và thêm trường id nữa
const transformDoc = (document) => {
  return { ...document.doc.data(), id: document.doc.id };
};

const lessons = {
        namespaced: true,
        state: {
          list: [],  // Array các object lesson (ko theo trật tự bài)
          unlockList: [], //Array các bài đã mở khoá
          playlistCount: {}, //lesson_id : 9
          playlist: {}, //lesson_id: Array - Lấy từ yt, nơi chứa list các video của các bài
          homeworks: {},
          playlistDetail: {}, //vidId: Object - Lấy từ firebase videosDetail
          channels: {}, //channelId: Object
          questions: {}, //vidId
          allQuestions: [],
        },
        getters: {
          getCount(state) {
            return state.list.length;
          },
          getList(state) {
            return state.list;
          },
          getLessonIdfromNumber: (state) => (number) => {
            if(!state.list.length) return null;
            else {
              let index = state.list.findIndex((lesson) => lesson.number == number);
              if(index){
                return state.list[index].id;
              }else return null;
            }
          },
          getLessonNumberfromId: (state) => (id) => {
            if(!state.list.length) return null;
            else {
              let index = state.list.findIndex((lesson) => lesson.id == id);
              if(index){
                return state.list[index].number;
              }else return null;
            }
          },
          getUnlockList(state) {
            return state.unlockList;
          },
          getPlaylist: (state) => (lessonId) => {
            return state.playlist[lessonId];
          },
          getFullPlaylist(state) {
            return state.playlist || null;
          },
          getPlaylistCount: (state) => (lessonId) => {
            if(state.playlist[lessonId]){
              return state.playlist[lessonId].length();
            }else return 0
          },
          getAllPlaylistCount(state) {
            if (state.playlist) {
              let count = 0;
              Object.keys(state.playlist).forEach((playlistId) => {
                count += state.playlist[playlistId].length;
              });
              return count;
            } else {
              return 0;
            }
          },
          getVideoFromId: (state) => (lessonId, videoId) => {
            if (lessonId && videoId && state.playlist[lessonId]) {
              const index = state.playlist[lessonId].findIndex(
                (vid) => vid.vidId == videoId
              );
              if (index >= 0) {
                return state.playlist[lessonId][index];
              } else return null;
            } else return null;
          },
      
          //Lấy videoId và lessonId tương ứng kế tiếp với dữ liệu input
          getNextVideoAndLessonFromId: (state) => (lessonId,videoId) =>{
            if(!state.playlist || !state.playlist[lessonId]){// phải có thì mới tiếp đc
              return null;
            }
      
            // các bài bị tráo lộn xộn trong state.list ko theo lessonNumber, còn video thì theo thứ tự
            let videoIndex = state.playlist[lessonId].findIndex(vid => vid.vidId == videoId);
            let lessonIndex = state.list.findIndex(lesson => lesson.id == lessonId); 
            let lessonNumber = Number(state.list[lessonIndex].number);
            const count = state.playlist[lessonId].length;
            if(videoIndex == count - 1){ //là vid cuối, vid kế nhảy sang bài mới
                if(lessonNumber == state.list.length -1 || !state.unlockList.includes(lessonNumber + 1)){ // bài hiện tại đã là bài cuối trong state.list hoặc bài ko đc mở khoá
                    return null;
                }else{ //bài hiện tại là bài giữa
                    let nextLessonNumber = lessonNumber + 1;
                    let nextLesson = state.list.find(lesson => lesson.number == nextLessonNumber);
                    let nextVideo = state.playlist[nextLesson.id][0]
                    return{
                      nextLesId: nextLesson.id,
                      nextVidId: nextVideo.vidId,
                      nextVidTitle: `Bài ${nextLesson.number}.1: ${nextVideo.title}`
                    }
                }
            }else{ // Vid ở giữa
                let nextVideo = state.playlist[lessonId][videoIndex + 1]
                return {
                  nextLesId: lessonId,
                  nextVidId: nextVideo.vidId,
                  nextVidTitle: `Bài ${lessonNumber}.${videoIndex + 2}: ${nextVideo.title}`
                }
            }
          },
      
          getPrevVideoAndLessonFromId: (state) => (lessonId,videoId) => {
            if(!state.playlist || !state.playlist[lessonId]){// phải có thì mới tiếp đc
              return null;
            }
      
            // các bài bị tráo lộn xộn trong state.list ko theo lessonNumber, còn video thì theo thứ tự
            let videoIndex = state.playlist[lessonId].findIndex(vid => vid.vidId == videoId);
            let lessonIndex = state.list.findIndex(lesson => lesson.id == lessonId); 
            let lessonNumber = Number(state.list[lessonIndex].number);
            const count = state.playlist[lessonId].length;
      
            if(videoIndex == 0){ //là vid đầu, vid trước nhảy sang về bài cũ
                if(lessonNumber == 1){ // bài trước không có
                    return null;
                }else{ //bài hiện tại là bài giữa, tức có bài trước
      
                    let prevLessonNumber = lessonNumber - 1;
                    let prevLesson = state.list.find(lesson => lesson.number == prevLessonNumber);
                    let prevVideo = state.playlist[prevLesson.id][state.playlist[prevLesson.id].length - 1];
                    return{
                      prevLesId: prevLesson.id,
                      prevVidId: prevVideo.vidId,
                      prevVidTitle: `Bài ${prevLesson.number}.${state.playlist[prevLesson.id].length}: ${prevVideo.title}`
                    }
                }
            }else{ // Vid ở giữa
                let prevVideo = state.playlist[lessonId][videoIndex - 1]
                return {
                  prevLesId: lessonId,
                  prevVidId: prevVideo.vidId,
                  prevVidTitle: `Bài ${lessonNumber}.${videoIndex}: ${prevVideo.title}`
                }
            }
          },
      
          getLessonFromId: (state) => (lessonId) => {
            if (state.list.length > 0 && lessonId) {
              const index = state.list.findIndex((les) => {
                return les.id == lessonId;
              });
              if (index >= 0) {
                return state.list[index];
              } else return null;
            } else return null;
          },
          getChannel: (state) => (activeVideo) => {
            if (state.channels && activeVideo) {
              let channelId = activeVideo.channelId;
              if (state.channels[channelId]) {
                return state.channels[channelId];
              } else return null;
            } else return null;
          },
          getGuide: (state) => (videoId) => {
            if (state.playlistDetail[videoId]) {
              return state.playlistDetail[videoId].guide;
            } else return null;
          },
          getDescription: (state) => (videoId) => {
            if (state.playlist[videoId]) {
              return state.playlist[videoId].description;
            } else return null;
          },
          //trả ra list leson dầy đủ thông tin của những lesson đã đc mở khóa
          getFilterList(state) {
            if (state.list.length && state.unlockList.length) {
              const list = state.list.filter((lesson) => {
                return state.unlockList.includes(Number(lesson.number));
              });
              return _.orderBy(list, ["number"], ["desc"]);
            }
          },
          getQuestionList: (state) => (videoId, type) => {
            if(!videoId) return [];
            if (type == "cur") {
              return state.questions[videoId];
            } else {
              return state.allQuestions;
            }
          },
      
          getLatestUnlockID(state, getters) {
            if (state.list.length && state.unlockList.length) {
              const filterList = getters["getFilterList"];
              return filterList[0].id;
            } else {
              return 0;
            }
          },
        },
        mutations: {
          resetLesson(state) {
            state.list = [];
            state.unlockList = [];
          },
          setList(state, list) {
            state.list = list;
          },
          setUnlockList(state, list) {
            state.unlockList = list;
          },
        },
        actions: {
          resetLesson({ commit }) {
            commit("resetLesson");
          },
          //lấy list lesson cùng có courseID của user, và unlock list ở class user đang học
          async firstLoadLesson({ commit, rootGetters, state }) {
            commit("resetLesson");
      
            const courseID = rootGetters["user/getCourseID"];
      
            const {
              dataArray: lessonList,
              error: err1,
              load: loadLesson,
            } = getCollectionFilter();
            await loadLesson("lessons", "courseID", courseID);
      
            if (lessonList.value.length > 0) {
              commit("setList", lessonList.value);
            }
          },
      
          async firstLoadUnlock({ commit, rootGetters }) {
            const classID = rootGetters["user/getClassID"];
            const {
              data: activeClass,
              error: err2,
              load: loadClass,
            } = getDoc("classes");
            await loadClass(classID);
            if (activeClass.value.unlockLessons.length > 0) {
              commit("setUnlockList", [...activeClass.value.unlockLessons]);
              //console.log("set unlock ",[...activeClass.value.unlockLessons]);
            }
          },
      
          async loadLesson({ state }, { lessonId }) {
            const index = state.list.findIndex((lesson) => lesson.id == lessonId);
            if (index >= 0) return state.list[index];
      
            const { data: lesson, error: err, load: loadLesson } = getDoc("lessons");
            await loadLesson(lessonId);
            if (!err.value) {
              state.list.push(lesson.value);
              return lesson.value;
            }
          },
          
          ///Load toàn bộ video của bài hiện tại và tống vào state.playlist 
          async loadPlaylist({ state, rootGetters, getters}, { playlistId, lessonId }) {
            // Nếu đã load đc playlist rồi thì trả về
            if (state.playlist[lessonId]) {
              return state.playlist[lessonId];
            }
            try {
              console.log(HTTP);
              const result = await HTTP.get(`api/playlist/${playlistId}/list`);
      
              if (!result.success) {
                console.log("Load playlist fail");
                return [];
              }
      
              state.playlist[lessonId] = result.data;

              //lấy videoRecords từ studentRecord chắp vào đầu tiên
              const records = rootGetters['class/getVideoRecords'];
              const lessonNumer = getters.getLessonNumberfromId(lessonId)
              
              if(records && records[lessonNumer]){
                const recordId = records[lessonNumer];
                if(!state.playlist[lessonId] || state.playlist[lessonId].length == 0){ // playlist chưa có phần tử nào
                    let recResult = await HTTP.get(`api/video/${recordId}`);
                    console.log(recResult);
                    if(recResult.success){
                      state.playlist[lessonId] = [recResult.data];
                    }
                }else{
                    let recResult = await HTTP.get(`api/video/${recordId}`);
                    console.log(recResult);
                    if(recResult.success){
                      state.playlist[lessonId].unshift(recResult.data);
                    }
                }
                //console.log(`🚀 ~ Bài ${lessonNumer} có record`, state.playlist[lessonId])
              }
              return state.playlist[lessonId];

            } catch (err) {
              console.log(err.message,state.playlist[lessonId]);
              return state.playlist[lessonId] || [];
            }
          },
          
          async loadVideosDetail({ state }, { videoId }) {
            if (state.playlistDetail[videoId]) {
              return state.playlistDetail[videoId];
            }
            const { data, error, load } = getDoc("videosDetail");
            //console.log("🚀 ~ videoId", videoId)
            await load(videoId);
            if (!error.value) {
              state.playlistDetail[videoId] = data.value;
              return data.value;
            } else return null;
          },
      
          //Load channel từ channelId
          async loadChannel({ state }, { channelId }) {
            if (state.channels[channelId]) {
              return state.channels[channelId];
            }
            const result = await HTTP.get(`api/channel/${channelId}`);
            if (result.success) {
              let channel = result.channel;
              state.channels[channelId] = channel;
              return channel;
            } else return null;
          },
      
          async loadQuestionVideo({ state }, { videoId }) {
            if (state.questions[videoId]) {
              return state.questions[videoId];
            }
            const { dataArray: quesArr, error, load } = getCollectionFilter();
            await load("questions", "videoID", videoId);
            if (!error.value) {
              state.questions[videoId] = quesArr.value;
              return quesArr.value;
            } else return [];
          },
      
          async loadQuestionAll({ state }, { courseID }) {
            if (state.allQuestions.length > 0) {
              return state.allQuestions;
            }
            const { dataArray: quesArr, error, load } = getCollectionFilter();
            await load("questions", "courseID", courseID);
            if (!error.value) {
              state.allQuestions = quesArr.value;
              return quesArr.value;
            } else return [];
          },
      
          async createQuestion({ state }, { newQues }) {
            const ques = { ...newQues, createdAt: timestamp() };
            const { error: errCreate, addDoc } = useCollection("questions");
            const res = await addDoc(ques);
            if (!errCreate.value) {
              toast.clear();
              toast.success(`Tạo câu hỏi mới thành công !`);
              ques.createdAt = "Vừa xong";
              ques.id = res.id;
              if (state.questions[ques.videoID]) {
                state.questions[ques.videoID].unshift(ques);
              } else state.questions[ques.videoID] = [ques];
      
              state.allQuestions.unshift(ques);
              return true;
            } else {
              toast.error(`Tạo câu hỏi mới thất bại!`);
              return false;
            }
          },
      
          async deleteQuestion({ state }, { quesId, videoId }) {
            const { error, remove } = removeDoc("questions");
            await remove(quesId);
            if (!error.value) {
              toast.success("Xoá câu hỏi thành công !");
      
              const index1 = state.questions[videoId].findIndex(
                (ques) => ques.id == quesId
              );
              state.questions[videoId].splice(index1, 1);
      
              const index2 = state.allQuestions.findIndex(
                (ques) => ques.id == quesId
              );
              state.allQuestions.splice(index2, 1);
              return true;
            } else {
              toast.error("Xoá câu hỏi thất bại");
              return false;
            }
          },
      
          async updateQuestion({ state }, { quesId, videoId, data }) {
            const { error, update } = updateDoc("questions");
            await update(quesId, data);
            if (!error.value) {
              toast.success(`Sửa câu hỏi thành công`);
      
              const index1 = state.questions[videoId].findIndex(
                (ques) => ques.id == quesId
              );
              state.questions[videoId][index1] = data;
      
              const index2 = state.allQuestions.findIndex(
                (ques) => ques.id == quesId
              );
              state.allQuestions[index2] = data;
      
              return true;
            } else {
              toast.error("Sửa câu hỏi thất bại");
              return false;
            }
          },
      
          setListenerLessons({ rootGetters, state }) {
            // khởi tạo listener chạy mỗi khi thay đổi data base
            const courseID = rootGetters["user/getCourseID"];
      
            projectFirestore
              .collection("lessons")
              .where("courseID", "==", courseID)
              .onSnapshot((snapshot) => {
                snapshot.docChanges().forEach((change) => {
                  const newDoc = transformDoc(change);
                  if (change.type === "added") {
                    state.list.unshift(newDoc);
                  }
                  if (change.type === "modified") {
                    const index = (state.list.findIndex((lesson) => {
                      return newDoc.id == lesson.id;
                    })(state.list)[index] = newDoc);
                  }
                  if (change.type === "removed") {
                    const index = state.list.findIndex((lesson) => {
                      return newDoc.id == lesson.id;
                    });
                    state.list.splice(index, 1);
                  }
                });
              });
          },
          setListenerUnlock({ rootGetters, commit }) {
            const classID = rootGetters["user/getClassID"];
            projectFirestore
              .collection("classes")
              .doc(classID)
              .onSnapshot((doc) => {
                commit("setUnlockList", doc.data().unlockLessons);
              });
          },
      
          // sửa trường editorDâta của lesson
          async updateHomework({ state }, { lessonID, data }) {
            const { error, update } = updateDoc("lessons");
      
            await update(lessonID, data);
            if (!error.value) {
              state.homeworks[lessonID] = data;
              toast.success(`Tạo bài tập thành công`);
              return true;
            } else {
              toast.error("Tạo bài tập thất bại");
              return false;
            }
          },
        },
};
export default lessons

