/*
Input: data:{content: "Hello"} Output cho ra dạng tương tự
*/
class Heading1 {
    static get isReadOnlySupported(){
        return true;
    }

    constructor({data,config}){
        this.data = data;
        this.config = config;
    }

    render(){
        //Chèn data
        let content = this.data && this.data.content ? this.data.content : '';
        const html = `
            <div class="hw-heading-1">
                <i class="material-icons icon">double_arrow</i>
                <h5 class="heading">${content}</h5>
            </div>
        `

        //Tạo html
        const wrapper = document.createElement('div');
        wrapper.innerHTML = html;
        wrapper.classList.add('hw-wrapper');
        
        // Gán sự kiện
        return wrapper;
    };
    save(blockContent){
        const element = blockContent.querySelector(".heading");
        const content = element.textContent;
        return{
            content: content
        }
    }
    validate(savedData){
        if(!savedData.content.trim()){
            return false
        } else return true;
    }
}

export default Heading1;