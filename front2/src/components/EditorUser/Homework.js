/*
Input: data:{content: "Hello"}
*/
import HTTP from '@/composable/axios.js'
import {downloadURL} from '@/composable/transformUrl'

class Homework {
    static get isReadOnlySupported(){
        return true;
    }
    static get toolbox(){
        return{
            title: 'Bài tập',
            icon: `
            <svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>
            `
        }
    }

    constructor({data,config}){
        this.data = data;
        this.config = config;
    }

    render(){
        //Chèn data
        const html = `
            <div class="hw-res">
                <div class="res-left">

                    <div class="thumb-container success">
                        <img class="thumb-img" src="${this.data.thumbUrl}" alt="">
                    </div>
                </div>
                <div class="res-right">
                    <div class="res-img">
                        <img src="/assets/png/resource-upload.png" alt="">
                    </div>
                    <a class="res-btn" href="${downloadURL(this.data.fileUrl)}">
                        <i class="material-icons">download_file</i>
                        <p> Tải về</p>
                    </a>
                </div>
            </div>
        `

        //Tạo html
        const wrapper = document.createElement('div');
        this.wrapper = wrapper;
        wrapper.innerHTML = html;
        wrapper.classList.add('hw-wrapper');

        return wrapper;
    };

    //đưa ảnh và file rar vào http post rồi bắn lên back
    save(){
        return null;
    }


    validate(savedData){
        if(savedData == null){
            return false
        } else return true;
    }
}

export default Homework;