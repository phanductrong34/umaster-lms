
import {Cloudinary} from 'cloudinary-core'
import HTTP from "@/composable/axios"

class Image {
    static get isReadOnlySupported(){
        return true;
    }
    static get toolbox(){
        return {
            title: 'Ảnh',
            icon: '<svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>'
        }
    }

    constructor({data}){
        this.data = data;
        this.photo = {},
        this.photo.secure_url = data.photoUrl,
        this.photo.public_id = data.photoId
    }


    render(){
        let html = `
        <div class="hw-image">
            <div class="container-image">
                <div class="result active">
                    <div class="img-wrapper">
                        <img class="img-result" src="" alt="">
                    </div>
                </div>
            </div>
        </div>
        `

        this.wrapper = document.createElement('div');
        this.wrapper.innerHTML = html;
        this.wrapper.classList.add('hw-wrapper');
        
        let contentEl = this.wrapper.querySelector('.content');
        let resultEl = this.wrapper.querySelector('.result')
        let pasteInput = this.wrapper.querySelector('.input-upload');
        let imageResult = this.wrapper.querySelector('.img-result');
        let btnWidget = this.wrapper.querySelector('.btn-widget');
        let btnDelete = this.wrapper.querySelector('.btn-delete');

        //LOAD IMAGE
        if(this.photo.secure_url && this.photo.public_id){
            imageResult.src = this.photo.secure_url;
        }

        return this.wrapper;
    }

    _savePhoto(file){
        this.photo = file;
    }
    save(){
        if(!this.photo || !this.photo.secure_url || !this.photo.public_id){
            return null;
        }
        return{
            photoUrl: this.photo.secure_url,
            photoId: this.photo.public_id
        }
    }


    validate(savedData){
        if(savedData == null){
            return false
        } else return true;
    }
}

export default Image