

class Video {
    static get toolbox(){
        return {
            title: 'Youtube',
            icon: `<svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.75 12.25V26.25L28 19.25L15.75 12.25ZM36.75 5.25H5.25C3.325 5.25 1.75 6.825 1.75 8.75V29.75C1.75 31.675 3.325 33.25 5.25 33.25H14V36.75H28V33.25H36.75C38.675 33.25 40.25 31.675 40.25 29.75V8.75C40.25 6.825 38.675 5.25 36.75 5.25ZM36.75 29.75H5.25V8.75H36.75V29.75Z" fill="black"/>
            </svg>`
        }
    }

    constructor({data}){
        this.data = data;
        this.videoURL = data.videoURL ||  null;
        this.videoId = data.videoId || null
    }


    render(){
        let html = `
        <div class="hw-youtube">
                <div class="content active">
                        <input class="input" type="text" placeholder="Paste link video youtube vào đây">
                </div>
                <div class="video-container">
                        <div class="delete">
                                <a class="waves-effect waves-light btn btn-delete">Xóa video</a>
                        </div>
                        <iframe class="iframe" src="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>
        </div>
        `

        this.wrapper = document.createElement('div');
        this.wrapper.innerHTML = html;
        this.wrapper.classList.add('hw-wrapper');
        
        let contentEl = this.wrapper.querySelector('.content');
        let resultEl = this.wrapper.querySelector('.video-container')
        let pasteInput = this.wrapper.querySelector('.input');
        this.pasteInput = pasteInput
        let videoResult = this.wrapper.querySelector('.iframe');
        let btnDelete = this.wrapper.querySelector('.btn-delete');

        //LOAD IMAGE
        if(this.videoURL && this.videoId){
            videoResult.src = `https://www.youtube.com/embed/${this.videoId}`;
            resultEl.classList.add('active')
            contentEl.classList.remove('active');
        }
        
        // Gán sự kiện

        // lấy content từ clipboard (videoURL) và chyển thành đúng dạng để embed

        pasteInput.onpaste = async (event) => {
            // use event.originalEvent.clipboard for newer chrome versions
            var items = await navigator.clipboard.readText();;
            console.log(items);
            if(items.length > 0){
                let index = items.indexOf("https://youtu.be/")
                if(index > -1){
                        console.log(items);
                        this.videoURL = items;
                        this.videoId = items.substring(17);
                        videoResult.src = `https://www.youtube.com/embed/${this.videoId}`;
                        resultEl.classList.add('active')
                        contentEl.classList.remove('active');
                }else{
                        pasteInput.value = "Format không hợp lệ"
                }
             }
        }

        //Xoa
        btnDelete.addEventListener("click",async ()=>{
            
            contentEl.classList.add('active')
            resultEl.classList.remove('active');
            this.pasteInput.value = null;
            this.videoId = null;
            this.videoURL = null;
        });

        

        return this.wrapper;
    }

    async destroy(){
        this.videoId = null;
        this.videoURL = null;
        this.pasteInput.value = null;
    }
    save(){
        if(!this.videoId || !this.videoURL){
            return null;
        }
        return{
            videoURL: this.videoURL,
            videoId: this.videoId
        }
    }


    validate(savedData){
        if(savedData == null){
            return false
        } else return true;
    }
}

export default Video