/*
Input: data:{content: "Hello"} Output cho ra dạng tương tự
*/
class Heading1 {
    static get toolbox(){
        return{
            title: 'Đầu mục',
            icon: '<svg width="25" height="25" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="arrowhead-right"><rect width="24" height="24" transform="rotate(-90 12 12)" opacity="0"/><path fill=#AA0F1E d="M18.78 11.37l-4.78-6a1 1 0 0 0-1.41-.15 1 1 0 0 0-.15 1.41L16.71 12l-4.48 5.37a1 1 0 0 0 .13 1.41A1 1 0 0 0 13 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 .01-1.27z"/><path fill=#AA0F1E d="M7 5.37a1 1 0 0 0-1.61 1.26L9.71 12l-4.48 5.36a1 1 0 0 0 .13 1.41A1 1 0 0 0 6 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 0-1.27z"/></g></g></svg>'
        }
    }

    constructor({data,config}){
        this.data = data;
        this.config = config;
    }

    render(){
        //Chèn data
        let content = this.data && this.data.content ? this.data.content : '';
        const html = `
            <div class="hw-heading-1">
                <i class="material-icons icon">double_arrow</i>
                <h5 class="heading" contenteditable="${this.config.editable || false}">${content}</h5>
            </div>
        `

        //Tạo html
        const wrapper = document.createElement('div');
        wrapper.innerHTML = html;
        wrapper.classList.add('hw-wrapper');
        
        // Gán sự kiện
        return wrapper;
    };
    save(blockContent){
        const element = blockContent.querySelector(".heading");
        const content = element.textContent;
        return{
            content: content
        }
    }
    validate(savedData){
        if(!savedData.content.trim()){
            return false
        } else return true;
    }
}

export default Heading1;