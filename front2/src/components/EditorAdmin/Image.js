
import {Cloudinary} from 'cloudinary-core'
import HTTP from "@/composable/axios"

class Image {
    static get toolbox(){
        return {
            title: 'Ảnh',
            icon: '<svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>'
        }
    }

    constructor({data}){
        this.data = data;
        this.photo = {},
        this.myWidget = null,
        this.photo.secure_url = data.photoUrl,
        this.photo.public_id = data.photoId
    }


    render(){
        let html = `
        <div class="hw-image">
            <div class="container-image">
                <div class="content active">   
                    <div class="text">Thêm ảnh bằng cách upload hoặc paste ảnh chụp màn hình</div>  
                    <div class="upload">
                        <a class="waves-effect waves-light btn btn-widget">Upload ảnh</a>
                    </div>
                    <div class="clip">
                        <div class="input-wrapper">
                            <input class="input-upload" type="text" placeholder="Paste ảnh vào đây">
                        </div>
                    </div>
                </div>
                <div class="result">
                    <div class="img-wrapper">
                        <div class="delete">
                            <span class="btn-delete a-btn-hover"> <i class="material-icons">close</i></span>
                        </div>
                        <img class="img-result" src="" alt="">
                    </div>
                </div>
                <div class="loading isLoading">
                    <span class="loader"></span>
                </div>
            </div>
        </div>
        `

        this.wrapper = document.createElement('div');
        this.wrapper.innerHTML = html;
        this.wrapper.classList.add('hw-wrapper');
        
        let contentEl = this.wrapper.querySelector('.content');
        let resultEl = this.wrapper.querySelector('.result')
        let pasteInput = this.wrapper.querySelector('.input-upload');
        let imageResult = this.wrapper.querySelector('.img-result');
        let btnWidget = this.wrapper.querySelector('.btn-widget');
        let btnDelete = this.wrapper.querySelector('.btn-delete');

        //LOAD IMAGE
        if(this.photo.secure_url && this.photo.public_id){
            imageResult.src = this.photo.secure_url;
            resultEl.classList.add('active')
            contentEl.classList.remove('active');
        }

        const cl = new Cloudinary({cloud_name: "umaster", secure: true});
        let myWidget = cloudinary.createUploadWidget({
            cloudName: 'umaster', 
            sources: ["local","google_drive","url"],
            uploadPreset: 'assets'}, async (error, result) => { 
                if (!error && result && result.event === "success") { 
                    this.photo = result.info;
                    imageResult.src = this.photo.secure_url;
                    resultEl.classList.add('active')
                    contentEl.classList.remove('active');
                }
            }
        )
        this.myWidget = myWidget
        
        // Gán sự kiện

        //Mo widget
        btnWidget.addEventListener("click",()=> {
            myWidget.open();
        });

        // lấy content từ clipboard và gửi tạo ra ảnh thumbnail

        pasteInput.onpaste = async (event) => {
            // use event.originalEvent.clipboard for newer chrome versions
            var items = (event.clipboardData  || event.originalEvent.clipboardData).items;
            console.log(JSON.stringify(items)); // will give you the mime types
            // find pasted image among pasted items
            var blob = null;
            for (var i = 0; i < items.length; i++) {
              if (items[i].type.indexOf("image") === 0) {
                blob = items[i].getAsFile();
              }
            }
            // load image if there is a pasted image
            if (blob !== null) {
              //Luu de upload
                this.file = blob

                let data = new FormData();
                data.append("file_upload", this.file);
                data.append("fileName", "anh");
                const result = await HTTP.post('api/file/upload/assets',data);
                
                if(result.success){
                    this._savePhoto(result.data)
                    resultEl.classList.add('active')
                    contentEl.classList.remove('active');

                    // Load len DOM
                    var reader = new FileReader();
                    reader.onload = function() {
                        imageResult.src = result.data.secure_url;
                    };
                    reader.readAsDataURL(blob);
                }

            }            
        }

        //Xoa
        btnDelete.addEventListener("click",async ()=>{
            let public_id = this.photo.public_id;
            const res = await HTTP.post('api/file/delete',{public_id} );
            if(!res.success){
                console.log("delete photo fail");
                return;
            }

            contentEl.classList.add('active')
            resultEl.classList.remove('active');

            this.photo = {};
        });

        

        return this.wrapper;
    }

    async destroy(){
        if(this.photo || this.photo.secure_url || this.photo.public_id){
            let public_id = this.photo.public_id;
            const res = await HTTP.post('api/file/delete',{public_id} );
            if(!res.success){
                console.log("delete photo fail");
                return;
            }
            this.photo = {};
        }
    }

    _savePhoto(file){
        this.photo = file;
    }
    save(){
        if(!this.photo || !this.photo.secure_url || !this.photo.public_id){
            return null;
        }
        return{
            photoUrl: this.photo.secure_url,
            photoId: this.photo.public_id
        }
    }


    validate(savedData){
        if(savedData == null){
            return false
        } else return true;
    }
}

export default Image