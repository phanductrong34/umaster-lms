/*
Input: data:{content: "Hello"}
*/
import HTTP from '@/composable/axios.js'
import {downloadURL} from '@/composable/transformUrl'

class Homework {
    static get toolbox(){
        return{
            title: 'Bài tập',
            icon: `
            <svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>
            `
        }
    }

    constructor({data,config}){
        this.data = data;
        this.config = config;
        this.photo = {},
        this.photoName =null;
        this.wrapper = null;
        this.resLeft = null;
        this.resRight = null;
        this.fileSrc = {};
        this.loaderPhoto = null;
        this.loaderFile = null;
    }

    render(){
        //Chèn data
        const html = `
        <div class="hw-res">
        <div class="res-left">


            <div class="thumb-container ready">

                <div class="loader-wrapper photo">
                    <span class="loader"></span>
                </div>

                <p class="thumb-title">File ảnh làm hình giới thiệu bài tập</p>
                <label for="thumbChoose" class="thumb-btn">Chọn ảnh</label>
                <input class="thumb-input" type="file" id="thumbChoose" accept="image/*">
            </div>

        </div>
        <div class="res-right">
            <div class="loader-wrapper file">
                <span class="loader"></span>
            </div>

            <label for="fileChoose" class="res-btn">
                <i class="material-icons">publish</i>
                <p> Upload</p>
            </label>
            <input class="file-input" type="file" id="fileChoose" accept="zip,application/octet-stream,application/zip,application/x-zip,application/x-zip-compressed">
        </div>
        `

        //Tạo html
        const wrapper = document.createElement('div');
        this.wrapper = wrapper;
        wrapper.innerHTML = html;
        wrapper.classList.add('hw-wrapper');


        this.resLeft = wrapper.querySelector(".res-left");
        this.resRight = wrapper.querySelector(".res-right");

        //Nếu mà có data load vào thì nạp data vào constructor và show giao diện có data
        if(this.data && this.data.fileUrl && this.data.thumbUrl){
            this.photo.secure_url = this.data.thumbUrl;
            this.photo.public_id = this.data.thumbId;  
            this.fileSrc.secure_url = this.data.fileUrl;
            this.fileSrc.public_id = this.data.fileId;
            this.fileSrc.bytes = this.data.fileSize || 0;
            this.photo.bytes = this.data.thumbSize || 0;
            this._showThumb();
            this._showFile();
            return wrapper;
        }

        //nếu ko có file load vào từ content thì mới gán sự kiện chọn
        this._eventPhoto();
        this._eventFile();

        return wrapper;
    };
    _eventPhoto(){
        const choosePhoto = this.wrapper.querySelector("#thumbChoose");
        this.loaderPhoto = this.wrapper.querySelector(".loader-wrapper.photo");

        //upload ảnh thumb lên ngay từ lúc này và lấy url về
        choosePhoto.addEventListener('change',async (event)=> {
            if(!event.target.files[0]){
                console.log("Please choose a image");
            }
            this._toggleLoader("photo");
            let data = new FormData();
            data.append("file_upload", event.target.files[0]);
            data.append("fileName", event.target.files[0].name);
            const result = await HTTP.post('api/file/upload/preset1',data);
            
            
            if(result.success){
                
                this._toggleLoader("photo");
                this.photo = result.data;
                this._showThumb();
            }
        })
    }
    _eventFile(){
        const chooseFile = this.wrapper.querySelector('#fileChoose');
        this.loaderFile = this.wrapper.querySelector(".loader-wrapper.file");
        //Sự kiện chọn file
        chooseFile.addEventListener('change', async (event) => {
            if(!event.target.files[0]){
                console.log("Please choose a file");
            }
            this._toggleLoader("file");
            let data = new FormData();
            data.append("file_upload", event.target.files[0]);
            data.append("fileName", event.target.files[0].name);
            const result = await HTTP.post('api/file/upload/preset2',data);
            
            
            if(result.success){
                this._toggleLoader("file");
                this.fileSrc = result.data;
                this._showFile();
            }
        })
    }

    _showThumb(){
        if(!this.photo){
            console.log("Null photo");
            return;
        }

        const successHtml = `
        <div class="thumb-container success">
            <img class="thumb-img" src="./assets/png/thumb-file.png" alt="">
            <div class="thumb-banner">
                <p class="thumb-name">anh1.png</p>
                <p class="thumb-size">30MB</p>
                <i class="thumb-delete material-icons">delete</i>
            </div>
        <div/>
        `
        this.resLeft.innerHTML  = successHtml;
        const thumbImage = this.resLeft.querySelector(".thumb-img");
        const thumbName = this.resLeft.querySelector(".thumb-name");
        const thumbSize = this.resLeft.querySelector('.thumb-size')


        thumbImage.src = this.photo.secure_url;
        thumbName.textContent = this.photo.original_filename;
        thumbSize.textContent = (Number(this.photo.bytes) / (1024*1024)).toFixed(2) + " MB"


        // Sự kiện xoá photo   
        this.wrapper.querySelector(".thumb-delete").addEventListener("click",()=> this._removePhoto());

    }
    
    _showFile(){
        const successHtml = `
        <i class="file-delete material-icons">delete</i>
        <a target="_blank" href="" class="res-img">
            <img class="res-icon" src="/assets/png/resource-upload.png" alt="">
        </a>
        <div class="file-data"><div>
        `
        this.resRight.innerHTML = successHtml;

        //DOM
        this.fileDown = this.wrapper.querySelector(".res-img");

        //SHOW
        const fileData = this.wrapper.querySelector(".file-data");
        fileData.textContent = (Number(this.fileSrc.bytes) / (1024*1024)).toFixed(2) + " MB"
        this.fileDown.setAttribute("href", downloadURL(this.fileSrc.secure_url))

        //EVENT
        this.resRight.querySelector(".file-delete").addEventListener('click', async() => this._removeFile());
    }

    _toggleLoader(type){
        if(type == "photo"){
            this.loaderPhoto.classList.toggle('show');
        }else{
            this.loaderFile.classList.toggle('show');

        }
        
    }

    //Xoá ảnh trên cloud và chèn lại html cũ
    async _removePhoto(){
        let public_id = this.photo.public_id;
        const res = await HTTP.post('api/file/delete', {public_id});
        if(!res.success){
            console.log("delete photo fail");
            return;
        }
        this.resLeft.innerHTML= `
        <div class="thumb-container ready">

            <div class="loader-wrapper photo">
                <span class="loader"></span>
            </div>

            <p class="thumb-title">File ảnh làm hình giới thiệu bài tập</p>
            <label for="thumbChoose" class="thumb-btn">Chọn ảnh</label>
            <input class="thumb-input" type="file" id="thumbChoose" accept="image/*">
        </div>
        `

        this._eventPhoto();
        this.photo = {};
    }

    //Tương tự xoá file trên cloud và chèn html cũ
    async _removeFile(){
        let public_id = this.fileSrc.public_id;
        const res = await HTTP.post('api/file/delete',{public_id} );
        if(!res.success){
            console.log("delete file fail");
            return;
        }

        this.resRight.innerHTML = `
        <div class="loader-wrapper file">
            <span class="loader"></span>
        </div>

        <label for="fileChoose" class="res-btn">
            <i class="material-icons">publish</i>
            <p> Upload</p>
        </label>
        <input class="file-input" type="file" id="fileChoose" accept="zip,application/octet-stream,application/zip,application/x-zip,application/x-zip-compressed">
        `
        this._eventFile();
        this.file = {};
    }



    //đưa ảnh và file rar vào http post rồi bắn lên back
    save(){
        if(!this.photo && !this.fileSrc){
            return null;
        }
        console.log(this.photo.secure_url)
        return{
            thumbUrl: this.photo.secure_url,
            thumbSize: this.photo.bytes,
            thumbId: this.photo.public_id,
            fileUrl: this.fileSrc.secure_url,
            fileSize: this.photo.bytes,
            fileId: this.fileSrc.public_id,
        }
    }


    validate(savedData){
        if(savedData == null){
            return false
        } else return true;
    }
}

export default Homework;