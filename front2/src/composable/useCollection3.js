import {projectFirestore} from "@/firebase/config"
import { ref } from 'vue'

const useCollection3 = (collection1,id1,collection2,id2,collection3)=> {
    const error = ref(null);
    const data = ref(null);
    const dataArray = ref([]);

    const loadCollection3 = async() => {
      try {
          const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).collection(collection3)
          .orderBy('createdAt','desc').get();
          dataArray.value = res.docs.map(doc => {
              return {...doc.data(), id: doc.id}
          })
      } catch (err) {
          error.value = err.message;
          console.log('Get error log: '+ err.value);
      }
   }

    const loadDoc3 = async(id3) => {
      try {
          const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).collection(collection3).doc(id3).get();
          if(res.exists){
              data.value = {...res.data(),id: res.id}
          }else throw Error("data not found");
      } catch (err) {
          error.value = err.message;
          console.log(`getDoc error from ${collection2}`);
          console.log(error.value);
      }
    }

    const addDoc3 = async (doc)=> {
        try{
            const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).collection(collection3).add(doc);
            return res
        }catch(err){
            error.value = err.message;
            console.log(error.value,collection2);
        }
    }

    const updateDoc3 = async(id3,data) => {
      try {
          // dùng set thì ta sẽ quyết định được id
          const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).collection(collection3).doc(id3).update(data);
          error.value = null;
      } catch (err) {
          error.value = err.message;
          console.log('Update error log: '+ err.message);
      }
   }

   const setDoc3 = async(id3,data) => {
    try {
        const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).collection(collection3).doc(id3).set(data);
        error.value = null;
    } catch (err) {
        error.value = err.message;
        console.log('Add error log: '+ error.value);
    }
  }

  const removeDoc3 = async(id3) => {
    try {
        // dùng set thì ta sẽ quyết định được id
        const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).collection(collection3).doc(id3).delete();
        error.value = null;
    } catch (err) {
        error.value = err.message;
        console.log('Update error log: '+ err.message);
    }
}

    return {error,data,dataArray,loadCollection3,loadDoc3, addDoc3,updateDoc3,setDoc3,removeDoc3}
}

export default useCollection3
