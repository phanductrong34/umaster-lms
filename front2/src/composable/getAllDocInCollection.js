import {projectFirestore} from "@/firebase/config"
import { ref } from 'vue'

const getAllDoc = (collection) => {
    const dataArray = ref(null);
    const error = ref(null);

    const load = async() => {
        try {
            const snapshot = await projectFirestore.collection(collection).get();
            if(snapshot){
                console.log(snapshot);

                dataArray.value = snapshot.docs.map(doc => {
                    return {id:doc.id, ...doc.data()}
                });

            }else throw Error("data not found");
        } catch (err) {
            error.value = err.message;
            console.log(`getAllDoc error from ${collection}`);
            console.log(error.value);   
        }
    }


    return {dataArray , error, load}
}

export default getAllDoc