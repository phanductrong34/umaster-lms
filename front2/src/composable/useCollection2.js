import {projectFirestore} from "@/firebase/config"
import { ref } from 'vue'

const useCollection2 = (collection1,id1,collection2)=> {
    const error = ref(null);
    const data = ref(null);
    const dataArray = ref([]);

    const loadCollection2 = async() => {
      try {
          const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2)
          .orderBy('createdAt','desc').get();
          dataArray.value = res.docs.map(doc => {
              return {...doc.data(), id: doc.id}
          })
      } catch (err) {
          error.value = err.message;
          console.log('Get error log: ' + collection1 + id1 + collection2+ err.value);
      }
   }

    const loadDoc2 = async(id2) => {
      try {
          const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).get();
          if(res.exists){
              data.value = {...res.data(),id: res.id}
          }else throw Error("data not found");
      } catch (err) {
          error.value = err.message;
          console.log(`getDoc error from ${collection2}`);
          console.log(error.value);
      }
    }

    const addDoc2 = async (doc)=> {
        try{
            const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).add(doc);
            return res
        }catch(err){
            error.value = err.message;
            console.log(error.value,collection2);
        }
    }

    const updateDoc2 = async(id2,data) => {
      try {
          // dùng set thì ta sẽ quyết định được id
          const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).update(data);
          error.value = null;
      } catch (err) {
          error.value = err.message;
          console.log('Update error log: '+ err.message);
      }
   }

   const setDoc2 = async(id2,data) => {
    try {
        const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).set(data);
        error.value = null;
    } catch (err) {
        error.value = err.message;
        console.log('Add error log: '+ error.value);
    }
  }

  const removeDoc2 = async(id2) => {
    try {
        // dùng set thì ta sẽ quyết định được id
        const res = await projectFirestore.collection(collection1).doc(id1).collection(collection2).doc(id2).delete();
        error.value = null;
    } catch (err) {
        error.value = err.message;
        console.log('Update error log: '+ err.message);
    }
}

    return {error,data,dataArray,loadCollection2,loadDoc2, addDoc2,updateDoc2,setDoc2,removeDoc2}
}

export default useCollection2
