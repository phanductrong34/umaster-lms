const router = require('express').Router();
const axios =  require('axios');
const cloudinary = require('cloudinary').v2;
const {response}  = require('express');
const multer = require('multer'); //parsing formdata from post
const upload = multer()

const {streamUpload, streamUploadMutiple} = require('../composables/streamUpload') 

router.get("/file/:id", async (req,res) => {
    try{
        
        res.json({
            success: true,
        })
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })  
    }
})

//upload file to folder

router.post('/file/upload/:preset',upload.single("file_upload"), async (req ,res) => {  //Trường file_upload trong formdata là 1 file
    try{    
        // hàm đẩy từ buffer của multer ra stream upload lên cloudinary
        let result = await streamUpload(req, req.params.preset);
        console.log(result);

        res.json({
            success: true,
            data: result
        })
        
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })    
    }
})

//Xoa 1 file
router.post('/file/delete', async (req, res) => {
    try{
        let result = await cloudinary.uploader.destroy(req.body.public_id)
        console.log(result);
        res.json({
            success: true,
            data: result
        });
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })    
    }
})

//Xoa 1 file
router.post('/file/deleteVideo', async (req, res) => {
    try{
        let result = await cloudinary.uploader.destroy(req.body.public_id, {resource_type: 'video'})
        console.log(result);
        res.json({
            success: true,
            data: result
        });
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })    
    }
})


module.exports = router;

