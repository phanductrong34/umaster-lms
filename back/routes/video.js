const router = require('express').Router();
const axios  = require('axios');
const {response} = require('express');
const youtubeChannel = require('../models/youtubeChannel')
const youtubeVideo = require('../models/youtubeVideo')


router.get('/channel/:channelId', async (req ,res) => { 
    try{
        //params cua queryString
        let params = {
            part: "snippet",
            id: req.params.channelId,
            key: process.env.YOUTUBEKEY
        }
        // Sinh ra queryString
        const esc = encodeURIComponent;
        let queryString = Object.keys(params).map(k => esc(k) + '=' + esc(params[k]) ).join('&');
        let URI = constant.ytURI + "channels?" + queryString;

        // request lay playlist
        const response = await axios.get(URI)
        const channels = response.data.items.map(channel => {
            return new youtubeChannel(
                channel.id,
                channel.snippet.title,
                channel.snippet.description,
                channel.snippet.thumbnails
            )
        })

        res.json({
            success: true,
            channel: channels[0],
        }) 
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })

    }
})

router.get("/video/:videoId", async (req, res) => {
    try{
        
        //params cua queryString
        let params = {
            part: "snippet",
            id: req.params.videoId,
            key: process.env.YOUTUBEKEY
        }
        // Sinh ra queryString
        const esc = encodeURIComponent;
        let queryString = Object.keys(params).map(k => esc(k) + '=' + esc(params[k]) ).join('&');
        let URI = constant.ytURI + "videos?" + queryString;

        // request lay playlist
        const response = await axios.get(URI);
        if(response.data.pageInfo.totalResults > 0){
            const vid = response.data.items[0];
            const video =  new youtubeVideo(
                vid.snippet.title,
                vid.id,
                vid.id,
                vid.snippet.channelId,
                vid.snippet.thumbnails,
                0,
                vid.snippet.channelTitle,
                vid.snippet.description,
                "rec"
            )
    
            res.json({
                success: true,
                data: video,
            }) 
        }else{
            res.status(500).json({
                success: false,
                message: "No Video Found or Video is Private"
            })
    
        }
    }catch(err){
        res.status(500).json({
            success: false,
            message: err.message
        })

    }
})



module.exports = router;

