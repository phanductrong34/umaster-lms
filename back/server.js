const express = require('express'); // core
const morgan = require('morgan');  // log ra terminal
const bodyParser = require('body-parser'); //parsing req
const multer = require('multer'); //parsing formdata from post
const upload = multer()
const cors = require('cors');  
const dotenv = require('dotenv');  // access env var
const cloudinary = require('cloudinary').v2;  // cloud storaga
const app = express();

//Middleware
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false})); //parsing form-urlencoded
app.use(bodyParser.json()); //parsing application/json
// app.use(upload.array());


//truy cập biến môi trường
dotenv.config();
global.constant = require('./util/constant')


//Config cloudinary
cloudinary.config({ 
    cloud_name: 'umaster', 
    api_key: process.env.CLOUDINARY_API_KEY, 
    api_secret: process.env.CLOUDINARY_API_SECRET,
    secure: true
});


//API
const playlistRoutes = require('./routes/playlist');
const fileRoutes = require('./routes/file');
const videoRoutes = require('./routes/video');


app.use("/api", playlistRoutes);
app.use("/api",fileRoutes);
app.use("/api", videoRoutes);

// GET test
app.get('/', (req, res) => {
    res.json({
        data: "Test Okay",
        cloudinary: cloudinary
    })
})



// Open port for server
app.listen(3000, (err) => {
    if(err){
        console.log(err);
    }else{
        console.log("Listening on port 3000");
    }
})

