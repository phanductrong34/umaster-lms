import axios from "axios";

 
const HTTP = axios.create({
    baseURL: "http://localhost:5001/umaster-resourse-page/us-central1/app/",
    // headers: {
    //     Authorization: "Bearer {token}"
    // }
})


// Add a response interceptor
HTTP.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response.data;
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  });

export default HTTP