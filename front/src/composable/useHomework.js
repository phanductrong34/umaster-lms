import {store} from '../store/index'
import {Cloudinary} from 'cloudinary-core'
import {ref,inject} from 'vue'
import Timecode from 'timecode-boss'
import {useToast} from 'vue-toastification'
import HTTP from "@/composable/axios"
import { useGlobalVar } from '../composable/useGlobalVar'

const useHomework = () => {
    
    // define cloud name
    const cl = new Cloudinary({cloud_name: "umaster", secure: true});
    const uploadResult = ref(null);
    const newWork = ref(null);
    const toast = useToast();
    const error1 = ref(null);
    const myWidget = ref(null);
    const isComplete = ref(false);

    //helper
    const styleDuration = (frameRate, frames)=>{
        let tc = new Timecode('00:00:00:00', frameRate);
        tc.setFrames(frames);
        return tc.toString().slice(3,8);
    }

    //dufng 
    const checkComplete = (goal, cur)=>{
        if(cur < goal) isComplete.value = false
        else isComplete.value = true;
        console.log("🚀 ~ isComplete.value", isComplete.value)
        return isComplete.value
    }

    //Upload Homework: upload video lên cloudinary trước rồi lấy url đáp cùng sang firebase
    const uploadConfig = (lessonNumber,countGoal,countCurrent) => {
        myWidget.value = cloudinary.createUploadWidget({
            cloudName: 'umaster', 
            sources: ["local","google_drive","url"],
            uploadPreset: 'preset_1'}, async (error, result) => { 
                uploadResult.value = null;
                if (!error && result && result.event === "success") { 
                    uploadResult.value = result.info;
                    newWork.value = await store.dispatch('works/uploadWork', {
                        videoUrl: uploadResult.value.secure_url,
                        thumbnailUrl: uploadResult.value.secure_url.slice(0,-3) + 'jpg',
                        workSize: uploadResult.value.bytes,
                        workName: uploadResult.value.original_filename,
                        publicId: uploadResult.value.public_id,
                        number: lessonNumber,
                        workDuration: styleDuration(uploadResult.value.frame_rate,uploadResult.value.nb_frames),
                        downloadUrl:`https://res.cloudinary.com/umaster/video/upload/fl_attachment/${uploadResult.value.path}`
            
                    })
                    //nếu upload thành công
                    let check = checkComplete(countGoal,countCurrent + 1);
                    if(check){
                        //Cập nhật số bài thành công trên studentRecord
                        await store.dispatch("user/updateCompleteLesson",{
                            lessonNumber: lessonNumber,
                            value: true
                        }); 
                    }

                    closeWidget();
                }
            }
        )
    }

    const openWidget = async () => {
        myWidget.value.open()
    }
    const closeWidget = async() => {
        myWidget.value.close();
    }

    //Delete: xoá bài tập ở firebase trước rồi mới xoá video cloudinary

    const deleteHomework = async(work,countGoal,countCurrent) => {
        const publicId = work.publicId;
        const lessonNumber = work.number;
        console.log("🚀 ~ publicId", publicId)
        const result1 = await HTTP.post(`api/file/deleteVideo`,{
            public_id: publicId
        })
        if(result1.data.success){
            const result2 = await store.dispatch('works/deleteWork',{workID: work.id,studentID: work.studentID});
            if(result2){ 
                let check = checkComplete(countGoal,countCurrent - 1);
                if(!check){
                    await store.dispatch("user/updateCompleteLesson",{
                        lessonNumber:lessonNumber,
                        value: false
                    }); 
                }
                return true
            }
            else {
                checkComplete(countGoal,countCurrent);
                return false
            };
        }else{
            console.log(result1.message);
            checkComplete(countGoal,countCurrent);
        }
        
    }
    return {uploadResult,uploadConfig,openWidget,deleteHomework,isComplete};

}



export default useHomework