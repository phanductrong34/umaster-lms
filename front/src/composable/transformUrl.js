const string = "upload/"

const downloadURL = (url) => {
    let index = url.indexOf(string);
    if(index < 0) return url;

    return url.slice(0,index + 6) + "/fl_attachment" + url.slice(index + 6);
}

export {downloadURL}