import axios from "axios";
const local = 'http://localhost:3000/'
const hosting = `https://us-central1-umaster-resourse-page.cloudfunctions.net/app`

 
const HTTP = axios.create({
    baseURL: local,
    // headers: {
    //     Authorization: "Bearer {token}"
    // }
})


// Add a response interceptor do axios tạo thêm 1 tầng bọc vào data nhận đc
// https://axios-http.com/docs/interceptors
HTTP.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response.data;
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  });

export default HTTP