
import { useToast } from "vue-toastification";

const toast = useToast();

const images = {
        namespaced: true,
        state: {
          imageList: {},
        },
        getters: {
          getImage: (state) => (refUrl) => {
            return state.imageList[refUrl] || null;
          },
        },
        mutations: {},
        actions: {
          addImage({ state }, { refUrl, photoURL }) {
            state.imageList[refUrl] = photoURL;
          },
        },
};
export default images


      