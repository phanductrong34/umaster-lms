import getAllDoc from "@/composable/getAllDocInCollection";
import getCollectionFilter from "@/composable/getCollectionFilter";
import getCollectionFilter2 from "@/composable/getCollectionFilter2";
import getDoc from "@/composable/getDoc";
import {
  timestamp
} from "@/firebase/config";

import updateDoc from "@/composable/updateDoc";
import useCollection from "@/composable/useCollection";
import { useToast } from "vue-toastification";

const toast = useToast();

const user = {
        namespaced: true,
        state: {
          isAdmin: false,
          isTeacher: false,
          currentUser: null,
          userData: null,
          studentRecord: null, //{}  đây là active thôi
          allStudentRecords: {}, //courseId : {} vi 1 student có thể học nhiều khoá 1 lúc, lưu lại để đỡ load nhiều
          allCourses: {}, // courseId: {}
          allLessons: {}, // courseid: [],
          isLoadAllCourse: false, // để hạn chế load đi load lại
          allTeachers: {}, //teacherID: {}
          continueCourse: null,
      
          nextVideoId: null,
          nextLessonId: null,
          nextVideotitle: null,
          firtLoadNextPrevVideo: false, // Check lần đầu load next video
      
          prevVideoId: null,
          prevLessonId: null,
          prevVideoTitle: null,
          firstLoadPrevVideo: false
          //activeVideoId thì ở trong studentRecord rồi
      
        },
        getters: {
          getFirstLoadNext(state){
            return state.firtLoadNextPrevVideo;
          },
          getIsAdmin(state) {
            return state.isAdmin;
          },
          getIsTeacher(state) {
            return state.isTeacher;
          },
          getCurrentUser(state) {
            return state.currentUser;
          },
          getUserData(state) {
            return state.userData ? state.userData : null;
          },
          getCourseID(state) {
            return state.userData ? state.userData.courseID : null;
          },
          getClassID(state) {
            return state.userData ? state.userData.classID : null;
          },
          getStudentRecord(state) {
            return state.studentRecord;
          },
          getActiveVideoId(state) {
            if (state.studentRecord) {
              return state.studentRecord.activeVideoID;
            } else return null;
          },
          getActiveLessonId(state) {
            if (state.studentRecord) {
              return state.studentRecord.activeLessonID;
            } else return null;
          },
          //NEXT VIDEO
          getNextVideoId(state){  
              return state.nextVideoId;
          },
          getNextVideoTitle(state){  
              return state.nextVideotitle;
          },
          getNextLessonId(state){  
              return state.nextLessonId;
          },
      
          // PREV VIDEO
          getPrevVideoId(state){  
              return state.prevVideoId;
          },
          getPrevVideoTitle(state){  
              return state.prevVideotitle;
          },
          getPrevLessonId(state){  
              return state.prevLessonId;
          },
      
      
          //lấy tổng số lượng video đã hoàn thành
          getAllCompleteCount(state) {
            if (state.studentRecord) {
              let count = 0;
              Object.keys(state.studentRecord.lessonComplete).forEach((number) => {
                count += state.studentRecord.lessonComplete[number].length;
              });
              return count;
            } else {
              return 0;
            }
          },
          getLessonNote: (state) => (lessonNumber) => {
            if (state.studentRecord && lessonNumber) {
              let filterNote = [];
              state.studentRecord.note.forEach((note) => {
                if (note.lessonNumber == lessonNumber) filterNote.push(note);
              });
              return filterNote;
            } else return [];
          },
          getAllNote(state) {
            if (state.studentRecord) {
              return state.studentRecord.note;
            }
          },
        },
        mutations: {
          changeUser(state, user) {
            if (user == null) state.currentUser = null;
            else state.currentUser = user;
          },
          changeAdmin(state, check) {
            state.isAdmin = check;
          },
          resetUser(state) {
            state.isAdmin = false;
            state.currentUser = null;
            state.userData = null;
            state.isTeacher = null;
            state.studentRecord = null;
            state.allLessons = {};
            state.allStudentRecords = {};
            state.continueCourse = null;
          },
        },
        actions: {
          //tạo ra record mới
          async createStudentRecord({ state }) {
            const { error, addDoc } = useCollection("studentRecord");
            const newRecord = await addDoc({
              studentID: state.currentUser.uid,
              classID: state.userData.classID,
              courseID: state.getters.courseID,
              createdAt: timestamp(),
              payment: true,
            });
            if (!error.value) {
              state.studentRecord == newRecord;
              return newRecord;
            }
          },
      
          //Tạo note mới , bằng cách ghi đè toàn bộ array cũ + object note mới vào trường note của studentRecord tương ứng
          async createNote(
            { state },
            { content, timeMark, videoID, videoTitle, lessonNumber, lessonID, id }
          ) {
            if (!state.studentRecord.note) {
              state.studentRecord.note = [];
            }
            const newNote = {
              content,
              timeMark,
              videoID,
              videoTitle,
              lessonNumber,
              lessonID,
              id,
            };
            state.studentRecord.note.unshift(newNote);
            const { error, update } = updateDoc("studentRecord");
            await update(state.studentRecord.id, {
              note: state.studentRecord.note,
            });
            if (!error.value) {
              toast.success("Lưu note mới thành công!");
              return true;
            } else {
              toast.error("Lưu note mới thất bại");
              return false;
            }
          },
      
          async updateNote({ state }, { content, id }) {
            let notes = state.studentRecord.note;
            let index = notes.findIndex((note) => note.id == id);
            if (index < 0) {
              toast.error("Sủa note thất bại");
              return false;
            }
            notes[index].content = content;
            const { error, update } = updateDoc("studentRecord");
            await update(state.studentRecord.id, {
              note: notes,
            });
            if (!error.value) {
              toast.success("Sửa note thành công");
              return true;
            } else {
              toast.error("Sửa note thất bại");
              return false;
            }
          },
      
          async deleteNote({ state }, { id }) {
            let notes = state.studentRecord.note;
            let index = notes.findIndex((note) => note.id == id);
            if (index < 0) {
              toast.error("Xoá note thất bại");
              return false;
            }
            notes.splice(index, 1);
            const { error, update } = updateDoc("studentRecord");
            await update(state.studentRecord.id, {
              note: notes,
            });
            if (!error.value) {
              toast.success("Xoá note thành công");
              return true;
            } else {
              toast.error("Xoá note thất bại");
              return false;
            }
          },
      
          //load ra đúng record học của học sinh với classID và studentID tương ứng
          async loadStudentRecord({ state, dispatch }, { studentID, classID }) {
            if(state.studentRecord) return state.studentRecord;
            const { dataArray: record, error, load } = getCollectionFilter2();
            await load("studentRecord", "studentID", studentID, "classID", classID);
      
            if (!error.value && record.value.length > 0) {
              state.studentRecord = record.value[0];
              return record.value[0];
            } else if (!record.value) {
              //Không có dữ liệu học viên
      
              state.studentRecord = null;
              const record = await dispatch("createStudentRecord");
              return record;
            } else return null;
          },
      
          updateNameAndPhone({ state }, { phone, fullname }) {
            state.userData.phone = phone;
            state.userData.fullname = fullname;
          },
      
          async updateUserData({ state, commit }, { user, isAdmin, isTeacher }) {
            // update user and admin state
            commit("changeUser", user);
            state.isAdmin = isAdmin;
            state.isTeacher = isTeacher;
      
            let collection = null;
            isAdmin || isTeacher
              ? (collection = "admins")
              : (collection = "students");
      
            const { data, error, load } = getDoc(collection);
            await load(user.uid);
      
            if (!error.value) {
              state.userData = data.value;
            }
          },
          async updateNewUser({ state, commit }) {
            state.userData.isNewUser = false;
            const { error, update } = updateDoc("students");
            await update(state.currentUser.uid, {
              isNewUser: false,
            });
            //console.log("update newUser to false")
          },
      
          // Update những trường active của vidoe hiện tại vào studentRecord
          // Cập nhật nextVideo data bằng cách nạp videoId và lessonId ở bài hiện tại rồi gọi 1 getters ở module lessons
          async updateActiveVideo({ state,dispatch}, { videoId, lessonId, sec, mode }) {
            if(!lessonId || !videoId){
              return null;
            }
      
            if(mode){ // mode = 1 tức load lần sau
                //UpdateOffline
                state.studentRecord.activeLessonID = lessonId;
                state.studentRecord.activeVideoID = videoId;
                state.studentRecord.activeSec = sec;
                
      
                //UpdateOnline
                const { error, update } = updateDoc("studentRecord");
                await update(state.studentRecord.id, {
                  activeLessonID: lessonId,
                  activeVideoID: videoId,
                  activeSec: sec,
                });
                if(!error.value){
                  dispatch('_setNextPrevVideo',{
                    lessonId: lessonId,
                    videoId: videoId
                  })
                }
            }else{ // mode = 0 tức load lần đầu
              dispatch('_setNextPrevVideo',{
                lessonId: lessonId,
                videoId: videoId
              })
              state.firtLoadNextPrevVideo = true;
            }
          },
          _setNextPrevVideo({state,rootGetters},{lessonId,videoId}){
            let dataNext = rootGetters['lessons/getNextVideoAndLessonFromId'](lessonId,videoId);
            let dataPrev = rootGetters['lessons/getPrevVideoAndLessonFromId'](lessonId,videoId);
            if(dataNext){ // ko phải null;
              state.nextVideoId = dataNext.nextVidId,
              state.nextLessonId = dataNext.nextLesId,
              state.nextVideotitle = dataNext.nextVidTitle
            }else{
              state.nextVideoId = null;
              state.nextLessonId = null;
              state.nextVideotitle = null;
            }
      
            if(dataPrev){ // ko phải null;
              state.prevVideoId = dataPrev.prevVidId,
              state.prevLessonId = dataPrev.prevLesId,
              state.prevVideotitle = dataPrev.prevVidTitle
            }else{
              state.prevVideoId = null;
              state.prevLessonId = null;
              state.prevVideotitle = null;
            }
          },
      
      
      
          // Chuyển video kế tiếp bằng cách dùng dữ liệu video kế tiếp nạp vào hàm updateActiveVideo
          async loadNextVideo({state,dispatch}){
            await dispatch('updateActiveVideo',{
              videoId:  state.nextVideoId,
              lessonId: state.nextLessonId,
              sec: 0
            });
          },
      
      
          //Đánh dấu video hoàn thành vào studentRecord hiện tại
          async activateCompleteVideo({state},{videoId, lessonNumber}){
            //UpdateOffline
            if(!state.studentRecord.lessonComplete){ // Chưa có trường lessonComplete
              state.studentRecord.lessonComplete = {}
              state.studentRecord.lessonComplete[lessonNumber] = [videoId];
            }else if(!state.studentRecord.lessonComplete[lessonNumber]){ //Chưa có video nào hoàn thành bài số tương ứng
              state.studentRecord.lessonComplete[lessonNumber] = [videoId];
            }else{ //Có video hoàn thành bài đó rồi
              state.studentRecord.lessonComplete[lessonNumber].push(videoId);
            }
            
            //Update Firebase
            const { error, update } = updateDoc("studentRecord");
            await update(state.studentRecord.id, {
              lessonComplete : state.studentRecord.lessonComplete
            });
            if(!error.value){
              toast.success("Hoàn thành video");
              return true;
            }else return false;
          },
      
          //Bỏ đánh dấu video hoàn thành vào studentRecord hiện tại
          async disableCompleteVideo({state},{videoId, lessonNumber}){
            //UpdateOffline
            console.log("disable video");
            state.studentRecord.lessonComplete[lessonNumber] = state.studentRecord.lessonComplete[lessonNumber].filter((vidId)=> vidId != videoId);
            
            //Update Firebase
            const { error, update } = updateDoc("studentRecord");
            await update(state.studentRecord.id, {
              lessonComplete : state.studentRecord.lessonComplete
            });
            if(!error.value){
              toast.info("Bỏ hoàn thành video");
              return true;
            }else return false;
          },
      
          //Đưa toàn bộ biến về 
          resetUser({ commit }) {
            commit("resetUser");
          },
      
          //Check xem học viên có đang học khoá nào active không
          async isActiveLearning({ state }, { studentId }) {
            if (state.userData) {
              if (state.userData.courseID && state.userData.classID) {
                return true;
              } else return false;
            } else {
              const { data, error, load } = getDoc("students");
              await load(studentId);
              if (!error.value) {
                if (data.value.courseID && data.value.classID) {
                  return true;
                } else return false;
              }
            }
          },
      
          async loadCourse({ state }, { courseId }) {
            //load offline truoc
            if (state.allCourses[courseId]) {
              return state.allCourses[courseId];
            }
            const { data, error: err1, load } = getDoc("courses");
            await load(courseId);
            if (!err1.value) {
              state.allCourses[courseId] = data.value;
              return data.value;
            } else {
              return null;
            }
          },
          
          async loadTeacher({ state }, { teacherId }) {
            if (state.allTeachers[teacherId]) {
              return state.allTeachers[teacherId];
            }
            console.log(teacherId);
            const { data: dataTeacher, error: err2, load } = getDoc("admins");
            await load(teacherId);
            if (!err2.value) {
              state.allTeachers[teacherId] = dataTeacher.value;
              return dataTeacher.value;
            } else return null;
          },
      
          async loadAllCourses({ state }) {
            if (state.isLoadAllCourse) {
              //đã load rồi
              return Object.value(state.allCourses);
            }
            const { dataArray, error, load } = getAllDoc("courses");
            await load();
            if (!error.value) {
              state.isLoadAllCourse = true;
              dataArray.value.forEach((doc) => {
                state.allCourses[doc.id] = doc;
              });
              return Object.value(state.allCourses); // return array chứ ko phải object để tiện xử lý
            } else {
              state.isLoadAllCourse = false;
              return null;
            }
          },
      
          async loadLessonFromCourse({ state }, { courseId }) {
            if (state.allLessons[courseId]) {
              return state.allLessons[courseId];
            }
            const { dataArray, error, load } = getCollectionFilter();
            await loadLesson("lessons", "courseID", courseId);
      
            if (!error.value) {
              state.allLessons[courseId] = dataArray.value;
              return dataArray.value;
            } else {
              return null;
            }
          },
      
          //Update bài đã hoàn thành vào đúng activeStudentRecord
          async updateCompleteLesson({ state }, { lessonNumber, value }) {
            console.log(state.studentRecord);
          const { update, error } = updateDoc("studentRecord");
            if (value) { //true - thêm bài mới hoàn thành
                if(!state.studentRecord.completeChapters){ //nếu chưa hề có trường này
                  await update(state.studentRecord.id,{
                    completeChapters: [lessonNumber]
                  });
                  if(!error.value){
                      state.studentRecord.completeChapters = [lessonNumber]
                      //toast thành công ở đây
                      toast.success("Hoàn thành bài tập của buổi số ", lessonNumber);
                  }
                  
                }else{ // Nếu đã có rồi
                  state.studentRecord.completeChapters.push(lessonNumber);
                  await update(state.studentRecord.id,{
                      completeChapters: state.studentRecord.completeChapters
                  });
                  if(!error.value){
                      toast.success("Hoàn thành bài tập của buổi số ", lessonNumber);
                  }
      
                }
      
            }else{ //false - Xóa bài đc hoàn thành
              let index = state.studentRecord.completeChapters.indexOf(lessonNumber)
              state.studentRecord.completeChapters.splice(index,1);
              await update(state.studentRecord.id,{
                completeChapters: state.studentRecord.completeChapters
              });        
            }
          },
      
      
          //Lấy dữ liệu về bài continue để học tiếp dựa vào userData
          async loadContinueData({state}){
            if(state.continueCourse) return state.continueCourse;
      
            let classId  = state.userData.classID;
            let courseId = state.userData.courseID;
      
            const  {data : curCourse , error: err1, load: loadCourse} = getDoc('courses');
            const  {data : curClass , error: err2, load: loadClass} = getDoc('classes');
      
            await Promise.all([loadCourse(courseId), loadClass(classId)])
            if(!err1.value && !err2.value){
              state.continueCourse = {
                courseName: curCourse.value.name,
                className: curClass.value.classID,
                thumbnail: curCourse.value.thumbImageURL,
                tag: curCourse.value.tag,
                format: curCourse.value.format
              }
              //Lấy bài active là lớn nhất
              console.log("lấy bài active");
              if(curClass.value.unlockLessons && curClass.value.unlockLessons.length > 0){
                let lessonNumber = Math.max(...curClass.value.unlockLessons);
                const {dataArray , error, load} = getCollectionFilter2();
                await load("lessons","courseID", courseId, "number", lessonNumber.toString());
                if(!error.value){
                  console.log(dataArray.value);
                  let lesson = dataArray.value[0]
                  state.continueCourse.lessonNumber = lesson.number;
                  state.continueCourse.lessonName = lesson.name;
                };
              }else{

              }
      
              return state.continueCourse
            }else{
              return null;
            }
          }
        },
};
export default user

