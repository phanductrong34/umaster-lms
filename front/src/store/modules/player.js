
import { useToast } from "vue-toastification";

const toast = useToast();

const player = {
        namespaced: true,
        state: {
          player: null,
          currentTime: 0,
          duration: 0,
        },
        getters: {
          getPlayer(state) {
            return state.player;
          },
          getCurrentTime(state) {
            return state.currentTime;
          },
          getDuration(state) {
            return state.duration;
          },
        },
        mutations: {
          resetPlayer(state) {
            state.player = null;
            state.currentTime = 0;
            state.duration = 0;
          },
          setCurrentTime(state, time) {
            state.currentTime = time;
          },
          setPlayer(state, player) {
            state.player = player;
          },
          setDuration(state, time) {
            state.duration = time;
          },
        },
};
export default player