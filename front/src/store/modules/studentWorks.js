// để lưu dữ liệu ở trang classroom
import getCollectionFilter from "@/composable/getCollectionFilter";

import { useToast } from "vue-toastification";

const toast = useToast();


const studentWorks = {
        namespaced: true,
        state: {
          studentList: [], // [student1, student2, student3]
          studentWorks: {}, //"studentID": [work1, work2]
          allWorks: [], // [work1, work2],
        },
        getters: {},
        mutations: {
          pushWork(state, work) {
            const index = state.studentList.findIndex(
              (student) => student.id == work.studentID
            );
            if (index >= 0) {
              state.studentList[index].works.push(work.id);
            }
            state.studentWorks[work.studentID].push(work);
            state.allWorks.push(work);
          },
          deleteWork(state, data) {
            const workID = data.workID;
            const studentID = data.studentID;
            const index1 = state.studentList.findIndex(
              (student) => student.id == studentID
            ); // tìm ra học sinh có bài tập ấy
            const index2 = state.studentWorks.findIndex((work) => work.id == workID); // tìm ra bài tập ấy trong studentWorks
            const index3 = state.allWorks.findIndex((work) => work.id == workID);
            if (index1 >= 0) {
              state.studentList[index1].works = state.studentList[
                index1
              ].works.splice(index1, 1);
            }
            if (index2 >= 0) {
              state.studentWorks[studentID] = state.studentWorks[studentID].splice(
                index2,
                1
              );
            }
            if (index3 >= 0) {
              state.allWorks.splice(index3, 1);
            }
          },
        },
        actions: {
          resetStudentWorks({ state }) {
            state.studentList = [];
            state.studentWorks = [];
            state.allWorks = [];
          },
          async getWorks({ state }, { studentID }) {
            const works = state.studentWorks[studentID];
            if (!works) {
              //get từ firestore
              const { dataArray, error, load } = getCollectionFilter();
              await load("works", "studentID", studentID);
              if (dataArray.value.length) {
                state.studentWorks[studentID] = dataArray.value;
                //console.log("🚀 ~ file: index.js ~ line 379 ~ dataArray.value", dataArray.value)
                return dataArray.value;
              } else return null; // fetch mà ko có work thì trả null
            } else {
              return works; // get mà có trong kho rồi thì không lấy nữa
            }
          },
          async getAllWorks({ state, rootGetters }) {
            if (!state.allWorks.length) {
              const classID = rootGetters["user/getClassID"];
              const { dataArray, error, load } = getCollectionFilter();
              await load("works", "classID", classID);
              if (dataArray.value.length) {
                state.allWorks = dataArray.value;
                return dataArray.value;
              } else return null;
            } else {
              return state.allWorks;
            }
          },
          async getStudentList({ state, rootGetters }) {
            if (!state.studentList.length) {
              const classID = rootGetters["user/getClassID"];
              const { dataArray, error, load } = getCollectionFilter();
              await load("students", "classID", classID);
              if (dataArray.value.length) {
                state.studentList = dataArray.value;
                //console.log("🚀 ~ file: index.js ~ line 403 ~ state.studentList", state.studentList)
                return state.studentList;
              }
            } else return state.studentList;
          },
        },
};
      
export default studentWorks