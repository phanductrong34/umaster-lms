import { createStore } from "vuex";

import admin from './modules/admin' 
import classes from './modules/classes'
import course from './modules/course'
import homework from './modules/homework'
import images from './modules/images'
import lessons from './modules/lessons'
import player from './modules/player'
import studentWorks from './modules/studentWorks'
import user from './modules/user'
import works from "./modules/works"

export const store = createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    player: player,
    user: user,
    lessons: lessons,
    course: course,
    class: classes,
    homework: homework,
    works: works,
    studentWorks: studentWorks,
    images: images,
    admin: admin,
  },
});
