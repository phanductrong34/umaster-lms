/*
Input: data:{
    unlockDate: timestamp,
    startTime: String // "6h45",
    timeRange: number //1
    quantity: number //1
}
*/
import uniqid from 'uniqid';

class Deadline {
    static get toolbox(){
        return{
            title: 'Deadline',
            icon: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><g data-name="Layer 2"><g data-name="attach-2"><rect width="24" height="24" opacity="0"/><path d="M12 22a5.86 5.86 0 0 1-6-5.7V6.13A4.24 4.24 0 0 1 10.33 2a4.24 4.24 0 0 1 4.34 4.13v10.18a2.67 2.67 0 0 1-5.33 0V6.92a1 1 0 0 1 1-1 1 1 0 0 1 1 1v9.39a.67.67 0 0 0 1.33 0V6.13A2.25 2.25 0 0 0 10.33 4 2.25 2.25 0 0 0 8 6.13V16.3a3.86 3.86 0 0 0 4 3.7 3.86 3.86 0 0 0 4-3.7V6.13a1 1 0 1 1 2 0V16.3a5.86 5.86 0 0 1-6 5.7z"/></g></g></svg>'
        }
    }

    constructor({data,config}){
        //this.homeworkID = data.homeworkID || null;
        this.unlockDate = data.unlockDate || Date.now();
        this.startTime = data.startTime || "6h45";
        this.timeRange = data.timeRange || 1
        this.config = config;
        this.wrapper = null;
        this.quantity = data.quantity || 1;
    }

    render(){
        //Chèn data
        const html = `
        <div class="hw-seperator"></div>
        <div class="hw-heading-3">
            <h5 class="fixed"></h5>
            <h5>&nbsp; Bài tập cần nộp</h5>
        </div>
        <div class="hw-quantity">
            <span class="a-btn-hover minus"> - </span>
            <div class="count">1</div>
            <span class="a-btn-hover plus"> + </span>
        </div>
        <div class="hw-picker">
            <span class="hw-option active">1 Tuần</span>
            <span class="hw-option">2 Tuần</span>
            <span class="hw-option">3 Tuần</span>
        </div>
        <div class="hw-deadline">
            <p class="hw-text"></p>
        </div>
        `


        //Tạo html
        this.wrapper = document.createElement('div');
        this.wrapper.innerHTML = html;

        this.wrapper.classList.add('hw-wrapper');

        //DOM
        this.fileChoose = this.wrapper.querySelector(".hw-upload");
        this.deadline = this.wrapper.querySelector(".hw-text");
        this.ranges = this.wrapper.querySelectorAll(".hw-option");
        this.plus = this.wrapper.querySelector('.plus');
        this.minus = this.wrapper.querySelector('.minus');
        this.count = this.wrapper.querySelector('.count')
        this.fixed = this.wrapper.querySelector('.fixed');


        //LOAD DATA

        if(this.timeRange && this.unlockDate){
            this._activeOption(this.timeRange-1);
        }
        this.count.textContent = this.quantity;
        this.fixed.textContent = this.quantity


        // Gán sự kiện
        this.ranges[0].addEventListener("click",() =>  this._activeOption(0));
        this.ranges[1].addEventListener("click", () => this._activeOption(1));
        this.ranges[2].addEventListener("click", () =>  this._activeOption(2));
        this.plus.addEventListener("click", ()=> {
            this.count.textContent = Number(this.count.textContent) + 1;
            this.fixed.textContent = this.count.textContent;
        })
        this.minus.addEventListener("click", ()=> {
            let count = Number(this.count.textContent);
            if(count > 1){
                this.count.textContent = Number(this.count.textContent) - 1;
                this.fixed.textContent = this.count.textContent;
            }
        })

        return this.wrapper;
    };
    _genDeadlineString(){
            let deadline = new Date (Number(this.unlockDate) + (86400000*7* Number(this.timeRange)));
            let days = ["Chủ Nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"];
            let day = days[deadline.getDay()];
        
            this.deadline.textContent = `
            Hạn nộp : ${this.startTime} - ${day} - ${deadline.getDate()}/${deadline.getMonth() +1}/${deadline.getFullYear()}
            `
    }

    _activeOption(index){
        let array = [0,1,2];
        array.splice(index,1);
        this.ranges.forEach((element)=>{
            element.classList.remove('active');
        })
        this.ranges[index].classList.add('active')
        this.timeRange = index+1
        //Tính ngày hạn
        this._genDeadlineString();
    }

    save(blockContent){
        return {
            unlockDate: this.unlockDate,
            timeRange: this.timeRange || 1,
            startTime: this.startTime,
            quantity: Number(this.count.textContent)
        }
    }
}

export default Deadline;