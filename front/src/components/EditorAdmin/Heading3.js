/*
Input: data:{content: "Hello"}
*/
class Heading3 {
    static get toolbox(){
        return{
            title: 'Tiêu đề',
            icon: `
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 489.42 489.42" style="enable-background:new 0 0 489.42 489.42;" xml:space="preserve">
            <path d="M416.435,0h-49.801c-18.201,0-32.963,14.753-32.963,32.963v153.883H155.748V32.963c0-18.21-14.76-32.963-32.963-32.963
                H72.982C54.781,0,40.02,14.753,40.02,32.963v423.494c0,18.209,14.761,32.963,32.962,32.963h49.803
                c18.203,0,32.963-14.754,32.963-32.963V302.574h177.924v153.883c0,18.209,14.762,32.963,32.963,32.963h49.801
                c18.203,0,32.965-14.754,32.965-32.963V32.963C449.4,14.753,434.638,0,416.435,0z"/>
            <g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
            </svg>
            `
        }
    }

    constructor({data,config}){
        this.data = data;
        this.config = config;
    }

    render(){
        //Chèn data
        let content = this.data && this.data.content ? this.data.content : '';
        const html = `
        <div class="hw-heading-3">
            <h5 data-text="${this.config.placeholder}" class="heading">${content}</h5>
        </div>
        `

        //Tạo html
        const wrapper = document.createElement('div');
        wrapper.innerHTML = html;
        wrapper.classList.add('hw-wrapper');
        
        // Gán sự kiện
        return wrapper;
    };
    save(blockContent){
        const element = blockContent.querySelector(".heading");
        const content = element.textContent;
        return{
            content: content
        }
    }
    validate(savedData){
        if(!savedData.content.trim()){
            return false
        } else return true;
    }
}

export default Heading3;