

class Video {
    static get isReadOnlySupported(){
        return true;
    }
    static get toolbox(){
        return {
            title: 'Youtube',
            icon: `<svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.75 12.25V26.25L28 19.25L15.75 12.25ZM36.75 5.25H5.25C3.325 5.25 1.75 6.825 1.75 8.75V29.75C1.75 31.675 3.325 33.25 5.25 33.25H14V36.75H28V33.25H36.75C38.675 33.25 40.25 31.675 40.25 29.75V8.75C40.25 6.825 38.675 5.25 36.75 5.25ZM36.75 29.75H5.25V8.75H36.75V29.75Z" fill="black"/>
            </svg>`
        }
    }

    constructor({data}){
        this.data = data;
        this.videoURL = data.videoURL ||  null;
        this.videoId = data.videoId || null
    }


    render(){
        let html = `
        <div class="hw-youtube">
                <div class="video-container active">
                        <iframe class="iframe" src="https://www.youtube.com/embed/${this.videoId}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>
        </div>
        `

        this.wrapper = document.createElement('div');
        this.wrapper.innerHTML = html;
        this.wrapper.classList.add('hw-wrapper');
        return this.wrapper;
    }

    save(){
        if(!this.videoId || !this.videoURL){
            return null;
        }
        return{
            videoURL: this.videoURL,
            videoId: this.videoId
        }
    }


    validate(savedData){
        if(savedData == null){
            return false
        } else return true;
    }
}

export default Video